<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# electron-placer

This crate contains multiple example placement engines for the LibrEDA-Rust framework.
The some placement algorithms implemented in this crate
simulate the movement of electric charges which are hold together by
the net connectivity.

See the documentation for more information.
 
## Documentation
Build and open the documentation with cargo:
```
cargo doc --open
```

Or find it [here](https://libreda.org/doc/electron_placer).
