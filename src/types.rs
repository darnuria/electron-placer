// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data types used in this crate.

pub use libreda_pnr::db::Vector;

use std::ops::{Add, AddAssign, Mul, Sub};
pub use num_traits::Zero;

/// Floating point type used for the computations in this crate.
pub type FloatType = f64;

pub type Vec2D = Vector<FloatType>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Net(pub usize);

/// Simplified representation of a standard-cell.
#[derive(Debug, Clone, PartialEq)]
pub struct Component {
    /// Nets to which the component is connected.
    pub nets: Vec<Net>,
    /// Width and height of the component.
    pub size: (f64, f64),
    /// Location of the component.
    pub location: Option<Vec2D>,
    /// Tell if the location of the component shall not be changed.
    pub fixed_location: bool,
}


#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Cost {
    pub cost: FloatType,
    pub gradient: Vec2D,
    pub hessian: (Vec2D, Vec2D),
}

impl Zero for Cost {
    fn zero() -> Self {
        Cost {
            cost: 0.,
            gradient: Vec2D::zero(),
            hessian: (Vec2D::zero(), Vec2D::zero()),
        }
    }

    fn is_zero(&self) -> bool {
        self.cost.is_zero()
            && self.gradient.is_zero()
            && self.hessian.0.is_zero()
            && self.hessian.1.is_zero()
    }
}

impl Add for Cost {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Cost {
            cost: self.cost + rhs.cost,
            gradient: self.gradient + rhs.gradient,
            hessian: {
                let (x1, y1) = self.hessian;
                let (x2, y2) = rhs.hessian;
                (x1 + x2, y1 + y2)
            },
        }
    }
}

impl Sub for Cost {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Cost {
            cost: self.cost - rhs.cost,
            gradient: self.gradient - rhs.gradient,
            hessian: {
                let (x1, y1) = self.hessian;
                let (x2, y2) = rhs.hessian;
                (x1 - x2, y1 - y2)
            },
        }
    }
}

impl AddAssign for Cost
{
    fn add_assign(&mut self, rhs: Self) {
        self.cost += rhs.cost;
        self.gradient += rhs.gradient;
        self.hessian.0 += rhs.hessian.0;
        self.hessian.1 += rhs.hessian.1;
    }
}

impl Mul<FloatType> for Cost {
    type Output = Self;
    fn mul(self, rhs: FloatType) -> Self {
        Cost {
            cost: self.cost * rhs,
            gradient: self.gradient * rhs,
            hessian: {
                let (x1, y1) = self.hessian;
                (x1 * rhs, y1 * rhs)
            },
        }
    }
}
