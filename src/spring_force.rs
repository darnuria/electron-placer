// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Calculate spring forces between particles.

use super::types::*;

fn spring_force(onto: Vec2D, from: Vec2D) -> Cost {
    let f = from - onto;
//    let df = f / f.norm2();
    debug_assert!(f.x.is_finite());
    debug_assert!(f.y.is_finite());
    Cost {
        cost: f.norm2(),
        gradient: f,
        hessian: (Vec2D::zero(), Vec2D::zero()),
    }
}

/// Compute the spring forces between this group of particles.
pub fn calculate_spring_forces(particle_locations: &Vec<Vec2D>) -> Vec<Cost> {
    let k = particle_locations.len();

    let weight = if k <= 1 {
        0.
    } else {
        1. / ((k - 1) as f64)
    };

    debug_assert!(weight.is_finite());

    particle_locations.iter().map(|&p| {
        // Sum up all spring forces to the other particles.
        particle_locations.iter().map(|&p2| {
            spring_force(p, p2)
        }).fold(Cost::zero(), |acc, v| acc + v) * weight
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_spring_force() {
        let locations = vec!(
            Vec2D::new(0., 0.),
            Vec2D::new(1., 0.)
        );

        let forces = calculate_spring_forces(&locations);

        assert_eq!(forces[0].gradient, Vec2D::new(1., 0.));
        assert_eq!(forces[1].gradient, Vec2D::new(-1., 0.));
    }
}