// Copyright (c) 2019-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Compute the sum of a sequence of floating point numbers with smaller error
//! than the naive approach.
//!
//! See: <https://en.wikipedia.org/wiki/Kahan_summation_algorithm>

use num_traits::Zero;
use std::ops::{Add, Sub, AddAssign};

/// Enable syntactically nice summation using the Kahan-Sum algorithm.
///
/// Usage: `let sum = iterator.kahan_sum()`
pub trait KahanSum<F> {
    /// Compute the sum of all iterator elements using error compensation.
    fn kahan_sum(self) -> F;
}

impl<I, F> KahanSum<F> for I
    where I: Iterator<Item=F>,
          F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    fn kahan_sum(self) -> F {
        kahan_sum(self)
    }
}

/// Accumulate floating point numbers using error compensation.
#[derive(Debug, Copy, Clone)]
pub struct KahanAccumulator<F> {
    /// Sum of the previous values.
    sum: F,
    /// Accumulator for errors.
    err: F,
}


impl<F> Default for KahanAccumulator<F>
    where F: Zero {
    /// Create an empty accumulator.
    fn default() -> Self {
        KahanAccumulator {
            sum: Zero::zero(),
            err: Zero::zero(),
        }
    }
}

impl<F> Zero for KahanAccumulator<F>
    where F: Zero + Copy + Sub<Output=F> + Add<Output=F> {
    fn zero() -> Self {
        KahanAccumulator {
            sum: Zero::zero(),
            err: Zero::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self.sum.is_zero() && self.err.is_zero()
    }
}

impl<F> KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    /// Create an empty accumulator.
    pub fn new() -> Self {
        Self::default()
    }

    /// Get the accumulated value.
    pub fn sum(&self) -> F {
        self.sum
    }

    /// Get the error term.
    #[allow(unused)]
    pub fn err(&self) -> F { self.err }

    /// Add the value to the accumulator.
    /// Returns a mutable reference to `self`.
    pub fn update(&mut self, value: F) -> &mut Self {
        let y = value - self.err;
        let t = self.sum + y;
        let err_next = (t - self.sum) - y;
        let sum_next = t;

        self.sum = sum_next;
        self.err = err_next;
        self
    }

    /// Consume the accumulator, add the value and return the updated accumulator.
    pub fn updated(mut self, value: F) -> Self {
        self.update(value);
        self
    }
}

impl<F> Add<F> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    type Output = Self;

    fn add(self, rhs: F) -> Self::Output {
        self.updated(rhs)
    }
}

impl<F> Sub<F> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    type Output = Self;

    fn sub(self, rhs: F) -> Self::Output {
        self.updated(F::zero() - rhs)
    }
}

impl<F> AddAssign<F> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    fn add_assign(&mut self, rhs: F) {
        self.update(rhs);
    }
}

impl<F> Add<Self> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    type Output = Self;

    fn add(mut self, rhs: Self) -> Self::Output {
        // TODO: Is this correct?
        self.err = self.err + rhs.err;
        self.updated(rhs.sum())
    }
}

impl<F> Sub<Self> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        // TODO: Is this correct?
        self.err = self.err - rhs.err;
        self.updated(F::zero() - rhs.sum())
    }
}

impl<F> AddAssign<Self> for KahanAccumulator<F>
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    fn add_assign(&mut self, rhs: Self) {
        // TODO: Is this correct?
        self.err = self.err + rhs.err;
        self.update(rhs.sum());
    }
}

/// Compute the sum of all elements in `iter`.
pub fn kahan_sum<I: Iterator<Item=F>, F>(iter: I) -> F
    where F: Zero + Add<Output=F> + Sub<Output=F> + Copy {
    let acc = iter.fold(
        KahanAccumulator::new(),
        |mut acc, input| {
            acc.update(input);
            acc
        },
    );

    acc.sum()
}

#[test]
fn test_kahan_sum() {
    let iter = vec![0.0f64, 1.1, 2.2].into_iter();
    let sum = kahan_sum(iter);
    assert!((sum - 3.3).abs() < 1e-9);
}

#[test]
fn test_kahan_sum_2() {
    let iter = vec![0.0f64, 1.1, 2.2].into_iter();
    let sum = iter.kahan_sum();
    assert!((sum - 3.3).abs() < 1e-9);
}

#[test]
fn test_kahan_sum_big() {
    let v = 1e-6;
    let n = 10000;
    let iter = (0..n).into_iter().map(|_| v);

    let sum = kahan_sum(iter.clone());
    let naive_sum = iter.fold(0., |acc, x| acc + x);

    assert!((sum - v * (n as f64)).abs() < 1e-100);
    assert!((naive_sum - v * (n as f64)).abs() > 1e-18);
}

#[test]
fn test_add_kahan_accumulators() {
    let v = 1e-6;
    let n = 10000;
    let iter = (0..n).into_iter()
        .map(|_| KahanAccumulator::new() + v);

    let sum: f64 = iter.fold(
        KahanAccumulator::zero(),
        |acc, x| acc + x)
        .sum();

    assert!((sum - v * (n as f64)).abs() < 1e-100);
}