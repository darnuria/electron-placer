// Copyright (c) 2019-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Two-dimensional FFT.

#![allow(unused)]

use rayon::prelude::*;

use rustfft::{FftPlanner, num_complex::Complex, Fft, FftDirection};
use ndarray::prelude::*;
use num_traits::{Float, Signed, FromPrimitive, Zero};
use transpose::transpose_inplace;
use std::sync::Arc;

pub trait FFTFloat: Float + std::fmt::Debug + Send + Sync + Signed + FromPrimitive + Default + 'static {}

impl<T: Float + std::fmt::Debug + Send + Sync + Signed + FromPrimitive + Default + 'static> FFTFloat for T {}

/// Shift the frequency domain data such that the DC value gets from the center to the corner.
/// For even sizes this is equivalent to `fftshift`.
pub fn ifftshift<T>(arr: &Array2<T>) -> Array2<T>
    where T: Clone + Zero {
    let (w, h) = arr.dim();
    debug_assert_eq!(w % 2, 0, "Require an even width.");
    debug_assert_eq!(h % 2, 0, "Require an even height.");
    fftshift(arr)
}

/// Shift the frequency domain data such that the DC value gets from the corner to the center.
/// Swaps half-axes for both dimensions.
pub fn fftshift<T>(arr: &Array2<T>) -> Array2<T>
    where T: Clone + Zero {
    let (w, h) = arr.dim();
    assert_eq!(w % 2, 0, "Require an even width.");
    assert_eq!(h % 2, 0, "Require an even height.");
    let mut result = Array2::zeros(arr.dim());

    let a = s![0..w/2, 0..h/2];
    let b = s![w/2..w, 0..h/2];
    let c = s![0..w/2, h/2..h];
    let d = s![w/2..w, h/2..w];

    result.slice_mut(a).assign(&arr.slice(d));
    result.slice_mut(d).assign(&arr.slice(a));

    result.slice_mut(b).assign(&arr.slice(c));
    result.slice_mut(c).assign(&arr.slice(b));

    result
}

/// Shift the frequency domain data such that the DC value gets from the center to the corner.
/// For even sizes this is equivalent to `fftshift`.
pub fn ifftshift_inplace<T>(arr: &mut Array2<T>) {
    let (w, h) = arr.dim();
    debug_assert_eq!(w % 2, 0, "Require an even width.");
    debug_assert_eq!(h % 2, 0, "Require an even height.");
    fftshift_inplace(arr);
}

/// Shift the frequency domain data such that the DC value gets from the corner to the center.
/// Swaps half-axes for both dimensions.
pub fn fftshift_inplace<T>(arr: &mut Array2<T>) {
    let (w, h) = arr.dim();
    assert_eq!(w % 2, 0, "Require an even width.");
    assert_eq!(h % 2, 0, "Require an even height.");

    for x in 0..w / 2 {
        for y in 0..h {
            let a = [x, y];
            let b = [(x + w / 2) % w, (y + h / 2) % h];
            arr.swap(a, b);
        }
    }
}

#[test]
fn test_fft_shift_inplace() {
    let mut arr = ndarray::array![
       [1, 0, 0, 2],
       [0, 0, 0, 0],
       [0, 0, 0, 0],
       [3, 0, 0, 4]
    ];
    fftshift_inplace(&mut arr);

    let expected = ndarray::array![
       [0, 0, 0, 0],
       [0, 4, 3, 0],
       [0, 2, 1, 0],
       [0, 0, 0, 0]
    ];
    assert_eq!(arr, expected);
}

/// Compute the two-dimensional FFT.
pub fn fft2<F>(arr: &Array2<Complex<F>>) -> Array2<Complex<F>>
    where F: FFTFloat {

    // Create a mutable copy.
    let mut arr_mut = arr.clone();
    fft2_forward_inplace(&mut arr_mut);
    arr_mut
}

/// Compute the two-dimensional inverse FFT.
pub fn ifft2<F>(arr: &Array2<Complex<F>>) -> Array2<Complex<F>>
    where F: FFTFloat {

    // Create a mutable copy.
    let mut arr_mut = arr.clone();
    ifft2_inplace(&mut arr_mut);
    arr_mut
}

/// Compute the two-dimensional FFT or inverse FFT.
pub fn fft2_inplace<F>(arr_mut: &mut Array2<Complex<F>>, direction: FftDirection)
    where F: FFTFloat {
    let (w, h) = (arr_mut.len_of(Axis(0)), arr_mut.len_of(Axis(1)));

    let mut planner = FftPlanner::new();
    // let mut planner = rustfft::FftPlannerAvx::new().expect("Failed to get AVX FFT planner.");
    // let mut planner = rustfft::FftPlannerSse::new().expect("Failed to get SSE FFT planner.");
    let row_fft = planner.plan_fft(h, direction);
    let col_fft = planner.plan_fft(w, direction);


    fft2_inplace_impl(row_fft, col_fft, arr_mut)
}

/// Compute the two-dimensional FFT with less allocation than `fft2`.
pub fn fft2_forward_inplace<F>(arr_mut: &mut Array2<Complex<F>>)
    where F: FFTFloat {
    fft2_inplace(arr_mut, FftDirection::Forward);
}

/// Compute the two-dimensional inverse FFT with less allocation than `fft2`.
/// Performs normalization.
pub fn ifft2_inplace<F>(arr_mut: &mut Array2<Complex<F>>)
    where F: FFTFloat {
    let (w, h) = (arr_mut.len_of(Axis(0)), arr_mut.len_of(Axis(1)));

    fft2_inplace(arr_mut, FftDirection::Inverse);

    // Normalize.
    let f = F::from_usize(w * h).unwrap();
    arr_mut.map_inplace(|e| *e = *e / f);
}

/// Compute the two-dimensional FFT/IFFT with little allocation.
fn fft2_inplace_impl<F>(row_fft: Arc<dyn Fft<F>>, col_fft: Arc<dyn Fft<F>>,
                        arr_mut: &mut Array2<Complex<F>>)
    where F: FFTFloat {
    debug_assert!(arr_mut.is_standard_layout(), "Array must be in standard 'C' layout.");

    let (w, h) = (arr_mut.len_of(Axis(0)), arr_mut.len_of(Axis(1)));

    // Scratch array for transpose.
    let mut transpose_scratch = vec![Complex::default(); w.max(h)];

    // Compute the FFT over each row.
    let row_scratch = vec![Complex::default(); row_fft.get_inplace_scratch_len()];
    arr_mut
        .axis_iter_mut(Axis(0))
        .into_par_iter() // Process in parallel.
        .for_each_with(row_scratch, |mut scratch, mut row| {
            let slice_mut = row.as_slice_mut()
                .expect("Wrong memory order of array elements.");
            row_fft.process_with_scratch(slice_mut, &mut scratch);
        });

    // Transpose the matrix and compute the FFT on the other axis.
    transpose_inplace(arr_mut.as_slice_mut().unwrap(),
                      &mut transpose_scratch,
                      h, w,
    );
    let mut arr_mut = arr_mut.view_mut().into_shape((h, w))
        .unwrap(); // Unwrap is fine because just w and h are swapped.

    // Compute the FFT over each column.
    let col_scratch = vec![Complex::default(); col_fft.get_inplace_scratch_len()];
    arr_mut
        .axis_iter_mut(Axis(0))
        .into_par_iter() // Process in parallel.
        .for_each_with(col_scratch, |mut scratch, mut col| {
            let slice_mut = col.as_slice_mut()
                .expect("Wrong memory order of array elements.");
            col_fft.process_with_scratch(slice_mut, &mut scratch);
            // col_fft.process(slice_mut);
        });

    // Transpose back.
    transpose_inplace(arr_mut.as_slice_mut().unwrap(),
                      &mut transpose_scratch,
                      w, h,
    );
}

#[test]
fn test_fft2() {
    // Compute fft and ifft, then check for equality.
    for i in 0..1 {
        let (w, h) = (16, 16);

        let mut arr = Array::from_elem((w, h), Complex { re: 0.0, im: 0.0 });
        arr[[0, 0]] = Complex { re: 1.0, im: 0.0 };

        let fft = fft2(&arr);
        let ifft = ifft2(&fft);

        // Check approximate equality.
        let err_sum: f64 = (arr - ifft).iter().map(|c| c.norm_sqr()).sum();
        assert!(err_sum < 1e-6);
    }
}
