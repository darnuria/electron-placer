// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Global placement engine. Finds positions of macros and standard-cells such that
//! the wire length is optimized and density constraints are met with little overlaps.
//!
//! Cells and macros do not get legalized.
//! After global placement the macros should be legalized (for example with the `AnnealingMacroLegalizer`.
//! During macro legalization the standard-cells are intended to be a soft-blockage: Overlap with them
//! should be minimized but is allowed.
//! After macro legalization the macros should be fixed and the standard-cells should be
//! placed again (using the current location as initial positions) and then legalized.

use rayon::prelude::*;

use libreda_pnr::db;
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};
use db::traits::*;
use libreda_pnr::db::{SimpleTransform, SimpleRPolygon};
use libreda_pnr::metrics::placement_density::DensityMap;

use std::collections::{HashMap, HashSet};

use super::fft2d;
use crate::kahan_sum::*;
use super::half_perimeter_wire_length::log_sum_exp_wire_length_gradient;
use crate::eplace_ms::half_perimeter_wire_length::{log_sum_exp_wire_length, log_sum_exp_wire_length_second_order_gradient};
use crate::eplace_ms::convolution_kernels::{electrostatic_potential_kernel, gradient_kernel, second_order_gradient_kernel};
use crate::eplace_ms::fft2d::FFTFloat;

// crates.io
use itertools::Itertools;
use ndarray::Array2;
use num_traits::{Zero, FromPrimitive, Float, NumAssign};
use rustfft::num_complex::Complex;
use log;
use rand::prelude::*;
use rand_pcg;
use rand_pcg::Pcg64;
use libreda_pnr::place::placement_problem::PlacementProblem;


/// Global mixed-size placement engine.
pub struct MixedSizeGlobalPlacer {
    /// Target placement density. Must be larger than zero and should be smaller than one.
    target_density: f64,
    /// Maximal number of placement iterations.
    max_iter: usize,
    /// FFT resolution. Approximate number of pixels per standard-cell.
    pixel_per_cell: f64,
}

impl MixedSizeGlobalPlacer {
    /// * `target_density`: Target placement density. Must be larger than zero and should be smaller than one.
    pub fn new(target_density: f64) -> Self {
        assert!(target_density > 0., "Target density must be larger than 0.");
        if target_density > 1. {
            // Warn about high target density.
            log::warn!("Target density is larger than 1: {}", target_density);
        }
        Self {
            target_density,
            max_iter: 200,
            pixel_per_cell: 1.0,
        }
    }

    /// Set maximum number of iterations.
    pub fn max_iter(&mut self, max_iter: usize) {
        self.max_iter = max_iter;
    }

    /// Set the relative FFT resolution in number of pixels per standard-cell (approximately).
    pub fn pixel_per_cell(&mut self, pixel_per_cell: f64) {
        assert!(pixel_per_cell > 0.0);
        self.pixel_per_cell = pixel_per_cell;
    }
}

/// For debugging.
/// Write a 2D array into a CSV file.
#[allow(unused)]
fn dump_array2<T: std::fmt::Display>(path: &str, v: &ndarray::Array2<T>) {
    // use std::io::Write;
    // log::info!("Write density map to {}.", path);
    // let mut f = std::fs::File::create(path).unwrap();
    //
    // let (w, h) = v.dim();
    //
    // for j in 0..h {
    //     for i in 0..w {
    //         write!(f, "{:}", v[[i, j]]);
    //         if i + 1 < w {
    //             write!(f, ", ");
    //         }
    //     }
    //     write!(f, "\n");
    // }
}

impl<C> MixedSizePlacer<C> for MixedSizeGlobalPlacer
    where C: db::L2NBase<Coord=db::Coord> {
    fn name(&self) -> &str {
        "ePlace_mGP"
    }

    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<C>,
    ) -> Result<HashMap<C::CellInstId, db::SimpleTransform<db::Coord>>, PlacementError> {
        let chip = placement_problem.fused_layout_netlist();

        // Get all cell outlines.
        let cell_outlines: HashMap<C::CellId, db::Rect<db::Coord>> = chip.each_cell()
            .flat_map(|cell_id| {
                placement_problem.cell_outline(&cell_id)
                    .map(|outline| (cell_id, outline))
            })
            .collect();
        let top_cell = placement_problem.top_cell();
        let placement_sites = placement_problem.placement_region();

        let top_ref = chip.cell_ref(&top_cell);

        let movable_instances: HashSet<_> =
            placement_problem.get_movable_instances();
        let fixed_instances: HashSet<_> =
            placement_problem.get_fixed_instances();

        // Fetch all net weights.
        let net_weights: HashMap<C::NetId, f64> = top_ref.each_net()
            .map(|net| (net.id(), placement_problem.net_weight(&net.id())))
            .collect();

        let initial_positions: HashMap<_, _> = movable_instances.union(&fixed_instances)
            .cloned()
            .map(|inst| {
                let pos = placement_problem.initial_position(&inst);
                (inst, pos)
            })
            .collect();

        // Find number of cells to be placed.
        let num_cells = top_ref.num_child_instances();

        if num_cells == 0 {
            // Trivial solution.
            // Return such that the rest of the program can assume there's at least one cell.
            return Ok(HashMap::new());
        }

        // Get the set of all instances relevant to this placement run.
        let all_instances: HashSet<_> = movable_instances.union(&fixed_instances)
            .cloned().collect();

        // // Create obstructions from fixed cells.
        // let obstructions = {
        //     let obstructions: Vec<db::Polygon<_>> = top_ref.each_cell_instance()
        //         .filter(|inst| !movable_instances.contains(&inst.id())) // Skip movable instances.
        //         .flat_map(|inst| { // ignore the cells that don't have an outline.
        //             let bbox = cell_outlines.get(&inst.template_id());
        //
        //             // Move bbox to actual location.
        //             bbox.map(|bbox| {
        //                 let pos = initial_positions.get(&inst.id())
        //                     .copied()
        //                     .unwrap_or_else(|| chip.get_transform(&inst.id()));
        //                 bbox.transform(|p| pos.transform_point(p))
        //             })
        //         })
        //         .map(|bbox| {
        //             bbox.into()
        //         })
        //         .collect();
        //     // Convert to multi-polygon.
        //     db::MultiPolygon::from_polygons(obstructions)
        // };

        // Find simplified pin locations of all the cells.
        let pin_locations = extract_pin_locations(chip);

        // Determine the size of the density raster.
        let core_area_bbox = placement_sites.iter().try_into_bounding_box()
            .ok_or_else(|| PlacementError::Other("Core area cannot be empty.".to_string()))?;

        // Grow the box to each side by 50%. This is needed to have a margin for
        // aliasing effects during the computation of the cyclic convolution.
        // (otherwise a electric force from the left edge would affect a cell on the right edge.)
        let universe_box = core_area_bbox.sized(core_area_bbox.width() / 2,
                                                core_area_bbox.height() / 2);
        log::info!("Core size: {:?}", core_area_bbox);
        log::info!("FFT region size: {:?}", universe_box);
        log::info!("Target density: {:.4}", self.target_density);

        // Find all cell areas and report cells which have no defined outlines.
        let mut cells_without_outlines = HashSet::new();
        let total_cell_area: f64 = {
            let areas = top_ref.each_cell_instance()
                .filter(|inst| all_instances.contains(&inst.id())) // Ignore the non-relevant instances.
                .map(|inst| {
                    let area = cell_outlines.get(&inst.template_id())
                        .map(|r| {
                            let r: db::Rect<f64> = r.cast();
                            r.area_doubled_oriented() / 2.
                        });
                    if area.is_none() {
                        cells_without_outlines.insert(inst.template().name());
                    }
                    area.unwrap_or(0.)
                });
            areas.kahan_sum()
        };

        // Print a warning if there are cells without defined outline.
        if !cells_without_outlines.is_empty() {
            log::warn!("No outline defined for cells: {}", cells_without_outlines.iter().sorted().join(", "));
        }

        // Find available core area.
        let available_core_area = placement_sites.iter()
            .map(|poly| {
                let poly: db::SimpleRPolygon<f64> = poly.cast();
                poly.area_doubled_oriented() / 2.
            })
            .kahan_sum();

        // Lower bound for placement density.
        let core_area_usage = total_cell_area / available_core_area;
        if core_area_usage > self.target_density {
            log::warn!("Core area usage is larger than target density: target density = {}", self.target_density);
        }
        log::info!("Core area usage: {:0.4}%", core_area_usage * 100.);

        // Find required raster resolution to resolve the cells and filler cells.
        // That is a rule of thumb.
        let (nx, ny) = {
            let smallest_cell_size: db::Rect<f64> = cell_outlines.values()
                .min_by_key(|r| r.width() as usize * r.height() as usize)
                .expect("No cell outlines given!")
                .cast();

            let bbox_area: f64 = universe_box.cast().area_doubled_oriented();

            // TODO: Use media/average cell size.
            let required_num_pixels = 0.25 * self.pixel_per_cell * bbox_area / smallest_cell_size.area_doubled_oriented();

            // Required raster side length for a square raster.
            let n_square = (required_num_pixels as f64).sqrt();

            // Adjust to the aspect ratio of the core bounding box.
            let (w, h) = (universe_box.width() as f64, universe_box.height() as f64);
            // Scale by 2 because the effective raster is twice as big to avoid aliasing effects.
            let nx = n_square * w / (w + h) * 2.;
            let ny = n_square * h / (w + h) * 2.;

            log::debug!("FFT raster size (not rounded): {}x{}", nx, ny);

            // Round up to next power of 2 to accelerate the FFT.
            // TODO: Find the optimal size for FFT (many small factors, etc...).
            let nx = (nx as usize).checked_next_power_of_two().expect("Failed to get next power of two.");
            let ny = (ny as usize).checked_next_power_of_two().expect("Failed to get next power of two.");

            log::debug!("FFT raster size (rounded up): {}x{}", nx, ny);

            (nx, ny)
        };

        // Rasterized data structure for representing cell densities.
        let density_bins: EDensity<f64> = EDensity::new(universe_box.cast(), (nx, ny));

        let mut rng = Pcg64::new(42, 0xa02bdbf7bb3c0a7ac28fa16a64abf96);

        // Create a list of all cells.
        let components = {
            let mut components = Vec::new();
            for cell in top_ref.each_cell_instance() {
                let is_movable = movable_instances.contains(&cell.id());
                let is_fixed = fixed_instances.contains(&cell.id());

                if !(is_movable || is_fixed) {
                    // Ingore cell.
                    continue;
                }

                // Find cell size.
                let outline_int: db::Rect<C::Coord> = {
                    if let Some(outline) = cell_outlines.get(&cell.template_id()) {
                        *outline
                    } else {
                        log::debug!("No outline defined for cell '{}'.", cell.template().name());

                        db::Rect::new(db::Point::zero(), db::Point::zero())
                    }
                };
                let outline: db::Rect<f64> = outline_int.cast();

                // Get initial position.
                let position = initial_positions.get(&cell.id()).cloned()
                    .unwrap_or_else(|| chip.get_transform(&cell.id()));

                let position = if !is_fixed {
                    // Apply a slight pseudo-random perturbation to break instable balances
                    // that could come from the initial placement. (For example when two cells
                    // with same shape perfectly overlap, or some cells are perfectly arranged in one
                    // row and no force is driving them apart.)

                    let perturbation: db::Vector<C::Coord> = {
                        let w = outline_int.width();
                        let h = outline_int.height();
                        let div = C::Coord::from_u32(8).unwrap();
                        let dx = w / div;
                        let dy = h / div;

                        db::Vector::new(
                            rng.gen_range(-dx..dx),
                            rng.gen_range(-dy..dy),
                        )
                    };

                    db::SimpleTransform {
                        displacement: position.displacement + perturbation,
                        ..position
                    }
                } else {
                    position
                };

                // Set the charge density equal to the target density for macro blocks.
                let (outline, charge_density) = {
                    // A block that spans for sure a full bin and more is considered a large macro.
                    let (bin_w, bin_h) = density_bins.charge_distribution.bin_dimension();
                    let is_large_block = outline.width() > 2. * bin_w && outline.height() > 2. * bin_h;
                    if is_large_block {
                        // let w = outline.width();
                        // let h = outline.height();
                        // let shrink_factor = 1. - (1.0 / self.target_density).sqrt();
                        // let outline_adapted = outline.sized(-w * shrink_factor / 2., -h * shrink_factor / 2.);
                        // // let charge_density = self.target_density * w * h /
                        // //     outline_adapted.width() * outline_adapted.height();
                        // (outline_adapted, 1.0)
                        (outline, self.target_density)
                    } else {
                        (outline, 1.0)
                    }
                };

                let component = Component {
                    id: Some(cell.id()),
                    size: outline,
                    charge_density,
                    position: position.cast(),
                    is_fixed,
                    is_filler: false,
                };

                components.push(component);
            }

            // Add top cell.
            // Adding the top cell is required to respect its pin shapes (if any) for the placement.
            components.push(Component {
                id: None, // None -> this is not a cell instance but the top cell.
                size: db::Rect::new(db::Point::zero(), db::Point::zero()),
                charge_density: 0.0, // Top cell is a virtual component and does not block its sub cells.
                position: SimpleTransform::identity(),
                is_fixed: true,
                is_filler: false,
            });

            components
        };

        // Create lookup tables for translating between indices and IDs.
        let component_by_id: HashMap<_, _> = components.iter()
            .map(|c: &Component<C>| c.id.clone()).enumerate()
            .map(|(i, c)| (c, i)).collect();

        let movable_components: Vec<usize> = components.iter()
            .enumerate()
            .filter(|(_, c)| !c.is_fixed)
            .map(|(i, _)| i)
            .collect();

        // // Pseudo randomly perturb components which are stuck on each other.
        // // (Perfectly overlapping components don't feel any electrostatic repulsion and could remain stuck together.)
        // {
        //     let mut stuck_components = HashMap::new();
        //     let mut stuck_locations = Vec::new();
        //     for (idx, c) in components.iter().enumerate() {
        //         let r: db::Rect<C::Coord> = c.size.transform(|p| c.position.transform_point(p)).cast();
        //         let entry = stuck_components.entry(r).or_insert(vec![]);
        //         if entry.len() == 1 {
        //             // Remember that there's more than one object at this exact position.
        //             stuck_locations.push(r);
        //         }
        //         entry.push(idx);
        //     }
        //     log::info!("Number of stuck components: {}", stuck_locations.len());
        //     for r in stuck_locations {
        //         let indices = &stuck_components[&r];
        //         for (c, &idx) in indices.iter().enumerate() {
        //             // Perturb location.
        //             let perturbation = db::Vector::new(c as f64, 0.);
        //             components[idx].position.displacement += perturbation;
        //         }
        //     }
        // }

        // For each net find all components that are connected to it
        // and also store the location of the pin relative to the component.
        let component_indices_by_net: HashMap<C::NetId, Vec<(usize, db::Point<f64>)>> = top_ref.each_net()
            .map(|net| {
                // Find all cells connected to this net.
                let connected_cell_instances: Vec<_> = net.each_terminal()
                    .filter_map(|t| {
                        let (cell_inst, pin) = match t {
                            TerminalRef::Pin(p) => (None, p),
                            TerminalRef::PinInst(i) => {
                                // Find location of the pin instance relative to the cell instance.
                                let pin = i.pin();
                                (Some(i.cell_instance()), pin)
                            }
                        };

                        if let Some(cell_inst) = &cell_inst {
                            // Ignore non-relevant cell instances.
                            if !all_instances.contains(&cell_inst.id()) {
                                return None;
                            }
                        }

                        // Find location of pin.
                        let cell = pin.cell();
                        let pin_location = pin_locations.get(&cell.id())
                            .and_then(|l| l.get(&pin.id()))
                            .copied()
                            .unwrap_or({
                                // Take the center of the cell as pin location if no pin shapes are defined.
                                cell_outlines.get(&cell.id())
                                    .map(|outline| outline.center())
                                    .unwrap_or((0, 0).into()) // Take (0, 0) as last resort.
                            })
                            .cast();

                        Some((cell_inst.map(|i| i.id()), pin_location))
                    })
                    // Convert to component indices.
                    .map(|(id, pin_loc)| (component_by_id[&id], pin_loc))
                    .collect();

                (net.id(), connected_cell_instances)
            })
            .collect();

        {
            let mut op: OptimizationProblem<C> = OptimizationProblem {
                components,
                movable_components,
                density: density_bins,
                net_weights: &net_weights,
                component_indices_by_net,
            };

            // Get initial values.
            let mut param: Vec<_> = op.movable_components.iter()
                .map(|&idx| op.components[idx].position.displacement).collect();

            // Optimization loop.

            // Weight of density cost.
            // This is low in the beginning and is steadily increased
            // towards the end of the optimization.
            let mut density_penalty = 0.00;

            // Main optimization loop.
            let mut prev_parameter_and_gradient: Option<(ndarray::Array1<db::Vector<f64>>, ndarray::Array1<db::Vector<f64>>)> = None; // v[k-1] and grad(f(v[k-1]))
            for i in 0..self.max_iter {
                log::info!("Iteration {}/{}", i + 1, self.max_iter);
                { // Rasterize for electro static force computation.
                    // Mark desired density distribution.
                    // TODO: Respect blockages here. Could be done by subtracting them from the core shape.
                    init_canvas(&mut op.density, &placement_sites, self.target_density);

                    // Update the densities with the cells (without fillers yet).
                    draw_components(&mut op.density, &op.components);

                    // 'Filler' insertion.
                    // Make the total charge zero by inserting positively charged fillers.
                    insert_filler_density(&mut op.density.charge_distribution);
                }


                { // Check stopping condition. Stop when density overflow is zero.

                    // Downsample the charge resolution: Compute average over larger window.
                    // let downsampling_factor = 2*((1. / self.target_density).sqrt()) as usize;
                    let downsampling_factor = 8;
                    let charge_distribution = op.density.charge_distribution.downsample(downsampling_factor);
                    let bin_area = charge_distribution.bin_area();

                    let overflow_threshold = 0.0; // Net charge in a bin is considered an overflow if larger than this value.

                    // Compute density overflow.
                    let density_overflow = charge_distribution.get_data_ref().iter()
                        .map(|v| v.re / bin_area)
                        .filter(|&v| v > overflow_threshold)
                        .kahan_sum();

                    let density_overflow_max: f64 =
                        charge_distribution.get_data_ref().iter()
                            .map(|v| v.re / bin_area)
                            .filter(|&v| v > overflow_threshold)
                            .map(|v| v - overflow_threshold)
                            .fold(0.0, |max, v| if v > max { v } else { max }); // Find the maximum value.

                    let num_overflows = charge_distribution.get_data_ref().iter()
                        .map(|v| v.re / bin_area)
                        .filter(|&v| v > overflow_threshold).count();

                    let density_overflow_avg = if num_overflows == 0 {
                        0.
                    } else {
                        density_overflow / num_overflows as f64
                    };

                    log::info!("Density overflow: avg={:.4}, max={:.4}", density_overflow_avg, density_overflow_max);

                    if density_overflow_max < 0.05 {
                        log::info!("Target density reached (within 5%).");
                        break;
                    }
                }

                // 1) Compute current gradient.
                let (energy, density_gradient, density_second_order_gradient)
                    = op.compute_electric_forces(&param);
                let (wire_length, wire_length_gradient, wire_length_second_order_gradient)
                    = op.compute_wire_forces(&param);

                debug_assert_eq!(density_gradient.len(), wire_length_gradient.len());
                debug_assert_eq!(density_second_order_gradient.len(), wire_length_second_order_gradient.len());

                log::info!("Wire length: {:.3} mm", wire_length / (chip.dbu() as f64) * 1e-3);
                log::debug!("Electrostatic energy: {}", energy);

                let (weight_wl, weight_d) = {
                    // Balance wirelength and density.

                    // Compute 2-norms of gradients.
                    let density_gradient_norm2: f64 =
                        density_gradient.iter()
                            .map(|v| v.norm2_squared())
                            .kahan_sum()
                            .sqrt();
                    let wire_length_gradient_norm2: f64 =
                        wire_length_gradient.iter()
                            .map(|v| v.norm2_squared())
                            .kahan_sum()
                            .sqrt();

                    let gradient_ratio = wire_length_gradient_norm2 / density_gradient_norm2;
                    log::debug!("gradient ratio (wirelength gradient / density gradient) = {:0.8}", gradient_ratio);

                    // Steadily increase the density penalty to converge to the target density.
                    density_penalty = 1. - (1. - density_penalty) * 0.99;

                    // let density_penalty = 1.0; // TODO
                    log::info!("density penalty = {:0.4}", density_penalty);

                    // let weight_d = density_penalty + (1. - density_penalty) * gradient_ratio;
                    let weight_d = density_penalty * gradient_ratio;

                    // Compute weights for wirelength and density.
                    // let weight_d = density_penalty;
                    let weight_wl = 1. - density_penalty;

                    log::info!("weight_wl = {:0.8}, weight_d = {:0.8}", weight_wl, weight_d);

                    (weight_wl, weight_d)
                };

                // Use weighted average of density penalty and wire length.
                let _cost = weight_wl * wire_length + weight_d * energy;
                let gradient: ndarray::Array1<db::Vector<f64>> =
                    wire_length_gradient * weight_wl
                        - density_gradient * weight_d;

                // Compute the second order gradient with the same weights.
                // The second order gradient will be used for the hessian pre-conditioning.
                let _second_order_gradient: ndarray::Array1<db::Vector<f64>> =
                    wire_length_second_order_gradient * weight_wl
                        - density_second_order_gradient * weight_d;

                // Convert optimization parameter into ndarray.
                let param_arr = ndarray::Array1::from_vec(param.clone());

                // 2) Hessian preconditioning.
                // let gradient = {
                //     let min_preconditioning = 0.1; // Make sure not to divide with very small numbers.
                //     let f = _second_order_gradient.iter()
                //         .map(|g| {
                //             // let norm = g.norm2();
                //             // norm.max(min_preconditioning)
                //             db::Vector::new(
                //                 g.x.abs().max(min_preconditioning),
                //                 g.y.abs().max(min_preconditioning),
                //             )
                //         });
                //     // Precondition the gradient vector.
                //     let mut gradient = gradient;
                //
                //     gradient.iter_mut()
                //         .zip(f)
                //         .for_each(|(g, f)| {
                //             g.x /= f.x;
                //             g.y /= f.y;
                //         });
                //     gradient
                // };

                // 3 ) Estimate Lipschitz constant.
                let lipschitz_constant = if let Some((prev_param, prev_gradient)) = prev_parameter_and_gradient {
                    // L = norm2(grad(f(v[k])) - grad(f(v[k-1]))) / norm2(v[k] - v[k-1])

                    let denom_sq: f64 = (&gradient - prev_gradient).iter()
                        .map(|v| v.norm2_squared())
                        .kahan_sum();
                    let nom_sq: f64 = (&param_arr - prev_param).iter()
                        .map(|v| v.norm2_squared())
                        .kahan_sum();

                    assert_ne!(nom_sq, 0., "Optimization parameter must differ between two iterations.");

                    denom_sq.sqrt() / nom_sq.sqrt()
                } else {
                    // Cannot yet tell since no iteration was done yet. Start pessimistic.
                    1e9 // TODO compute from second order gradient.
                };

                log::info!("Lipschitz constant L = {}", lipschitz_constant);

                let step_size = 0.8 / lipschitz_constant;
                // Update optimization parameter.
                param.iter_mut().zip(gradient.iter())
                    .for_each(|(p, grad)| {
                        *p = *p - *grad * step_size
                    });

                prev_parameter_and_gradient = Some((param_arr, gradient));

                // Limit component positions to core bounding box.
                let boundary: db::Rect<f64> = core_area_bbox.cast();
                for (&c, pos) in op.movable_components.iter().zip(param.iter_mut()) {
                    debug_assert!(!op.components[c].is_fixed);

                    // Add tiny random perturbation.
                    {
                        let perturbation: db::Vector<f64> = {
                            let r = op.components[c].size;
                            let w = r.width();
                            let h = r.height();
                            // Move the cell at most by a fraction of its width and height.
                            let dx = w * 0.05;
                            let dy = h * 0.05;

                            db::Vector::new(
                                rng.gen_range(-dx..dx),
                                rng.gen_range(-dy..dy),
                            )
                        };

                        *pos += perturbation;
                    }

                    // Limit position to core area box.
                    op.components[c].position.displacement = *pos;
                    let r = op.components[c].actual_shape();
                    // Find shift amount to bring the rectangle into the boundary box.
                    let mut correction = db::Vector::zero();

                    correction.x += (boundary.lower_left.x - r.lower_left.x).max(0.);
                    correction.x += (boundary.upper_right.x - r.upper_right.x).min(0.);
                    correction.y += (boundary.lower_left.y - r.lower_left.y).max(0.);
                    correction.y += (boundary.upper_right.y - r.upper_right.y).min(0.);

                    let limited_pos = *pos + correction;
                    *pos = limited_pos;

                    op.components[c].position.displacement = limited_pos;
                }


                // Update component locations.
                for (&c, &pos) in op.movable_components.iter().zip(&param) {
                    debug_assert!(!op.components[c].is_fixed);
                    op.components[c].position.displacement = pos;
                }
            }

            // Create result hash-map.
            let result: HashMap<_, _> = op.components.iter()
                .filter(|c| !c.is_fixed) // Don't output locations of fixed instances.
                .filter_map(|comp| {
                    let pos = comp.position.cast(); // Convert to data base coordinates.
                    comp.id.as_ref().map(|id| (id.clone(), pos)) // Skip the location of the top cell (has id = None).
                })
                .collect();

            Ok(result)
        }
    }
}


/// Compute the weighted forces (wire length gradient) on the components caused by all the nets
/// and the total wire-length.
/// Both forces and wire-length are weighted with the weights of the nets.
/// Compute the gradient of `positions`.
///
/// * `nets`: Iterator all nets. Produces `(net_weight, terminal_location)` tuples.
/// * `I`: Iterator over `(net weight, net components)` tuples.
/// * `positions`: Optimization parameter:
/// * `components`: List of components. Needed to find the pin positions.
/// * `gradient_buffer`: Storage for the resulting forces for each component. Must be the same length as `components`.
/// * `second_order_gradient_buffer`: Storage for the resulting derivative of the force for each component. Must be the same length as `components`.
/// * `scratch`: Temporary buffer.
///
/// Returns the total wire length.
fn compute_wirelength_and_gradient<'a, I: Iterator<Item=(f64, &'a Vec<(usize, db::Point<f64>)>)>, C: L2NBase>(
    nets: I,
    components: &Vec<Component<C>>,
    positions: &Vec<db::Vector<f64>>,
    gradient_buffer: &mut Vec<db::Vector<f64>>,
    second_order_gradient_buffer: &mut Vec<db::Vector<f64>>) -> f64 {
    assert_eq!(components.len(), positions.len());
    assert_eq!(components.len(), gradient_buffer.len());
    assert_eq!(components.len(), second_order_gradient_buffer.len());
    debug_assert!(positions.iter().all(|p| p.x.is_finite() && p.y.is_finite()), "Positions cannot be inf or NaN.");

    // TODO: choose gamma properly. Depends heavily on the database unit and the size of the core.
    let gamma = 10000.;

    // Save some allocations with reusable scratch space.
    let mut scratch = vec![];

    debug_assert_eq!(components.len(), gradient_buffer.len());
    // Initialize all forces to zero.
    gradient_buffer.iter_mut().for_each(|v| *v = Zero::zero());

    let mut wirelength_sum = KahanAccumulator::new();

    for (weight, components_of_net) in nets {
        // Find pin locations.
        let points = components_of_net.iter()
            .map(|&(c, pin_location)| {
                let pos = positions[c]; // Get current location of the cell.
                let tf = db::SimpleTransform { // Keep rotation/mirroring but use current location.
                    displacement: pos,
                    ..components[c].position
                };
                // Compute the actual position of the center of the cell.
                tf.transform_point(pin_location)
            });

        let (wl_x, wl_y) = log_sum_exp_wire_length(points.clone(), gamma);
        wirelength_sum.update((wl_x + wl_y) * weight);

        // Compute forces of this net.
        scratch.clear();
        log_sum_exp_wire_length_gradient(
            points.clone(),
            gamma,
            &mut scratch,
        );

        // Accumulate forces.
        for ((c, _pos), &f) in components_of_net.iter().zip(scratch.iter()) {
            gradient_buffer[*c] += f * weight;
        }

        // Compute second order gradient of wire-length approximation.
        scratch.clear();
        log_sum_exp_wire_length_second_order_gradient(
            points,
            gamma,
            &mut scratch,
        );
        // Accumulate.
        for ((c, _pos), &f) in components_of_net.iter().zip(scratch.iter()) {
            second_order_gradient_buffer[*c] += f * weight;
        }
    }

    scratch.clear();

    wirelength_sum.sum()
}

/// 'Filler' insertion.
///
/// Insert positively charged fillers such that the total charge gets zero.
///
/// Instead of putting filler cells as ePlace-MS does,
/// we directly update the negative-density bins like there
/// would be nicely distributed fillers. Bins with positive charge density
/// don't get changed.
///
/// * `density_bins`: Bins to be updated.
fn insert_filler_density(density_bins: &mut DensityMap<f64, Complex<f64>>) {
    let charge_sum: f64 = density_bins.get_data_ref().iter()
        .map(|v| v.re)
        .kahan_sum();

    if charge_sum > 0. {
        log::error!("Target density is too low and cannot be reached.");
    }
    assert!(charge_sum <= 0., "Cannot compensate negative charge by inserting negatively charged fillers.");

    {
    let charge_underflow = -charge_sum.min(0.); // That amount needs to be added.

    // Compute the sum of all negative bins.
    let negative_charge_sum: f64 = kahan_sum(
        density_bins.get_data_ref()
            .iter()
            .filter(|&&v| v.re < 0.)
            .map(|v| v.re)
    );

    let negative_charge_sum = -negative_charge_sum;
        // Add positive charges to negative bins.
        // Total amount of added charge = filler_area.
        density_bins.get_data_ref_mut()
            .iter_mut()
            .for_each(|v| {
                if v.re < 0.0 {
                    let abs = -v.re;
                    debug_assert!(abs >= -1e-9);
                    // Fill with positive charge proportional to the current negativeness.
                    v.re += charge_underflow * abs / negative_charge_sum;
                }
            });
    }

    // {
    //     // Distribute missing charge equally on all bins.
    //     let num_bins = density_bins.get_data_ref().len();
    //
    //     let charge_increment = -charge_sum / (num_bins as f64);
    //     density_bins.get_data_ref_mut()
    //         .iter_mut()
    //         .for_each(|v| {
    //             v.re += charge_increment;
    //         });
    // }

    { // Check that filler insertion properly balanced the charges.

        let total_charge: f64 = kahan_sum(density_bins.get_data_ref().iter().map(|v| v.re));

        assert!(
            total_charge < 1e-3,
            "Total charge must be zero but is {}.", total_charge
        );
    }
}

/// Representation of a cell to be placed.
#[derive(Clone, PartialEq)]
struct Component<C: L2NBase> {
    /// Id of the cell instance.
    /// `None` is reserved for the top cell.
    id: Option<C::CellInstId>,
    /// Bounding box of the cell template.
    size: db::Rect<f64>,
    /// The charge density of this component. This should b `1.0` for standard cells
    /// and `target_density` for larger blocks.
    charge_density: f64,
    /// Current position and orientation of the cell.
    position: db::SimpleTransform<f64>,
    /// Is this a non-movable cell?
    is_fixed: bool,
    /// This is a filler cell.
    is_filler: bool,
}

impl<C: L2NBase> Component<C> {
    /// Bounding box of placed component.
    fn actual_shape(&self) -> db::Rect<f64> {
        self.size.transform(|p| self.position.transform_point(p))
    }
}


/// Draw all cells onto the canvas with density = 1.
fn draw_components<C: L2NBase>(density: &mut EDensity<f64>, cells: &Vec<Component<C>>) {
    let boundary = density.charge_distribution.dimension;
    for cell in cells {
        // Move to actual place.
        if cell.is_filler || cell.id.is_some() { // Don't draw the top cell. Compensate for bug - zero-sized rectangle of the top-cell is drawn wrong.
            let shape = cell.actual_shape();
            assert!(boundary.contains_rectangle(&shape));
            density.draw_charge_density(&shape, cell.charge_density);
        }
    }
}

/// Clear the canvas and draw the region where cells should be placed.
/// The canvas represents a density requirement map: Where no cells should be placed
/// the value is zero, where cells should be placed with density `d` the value is `-d`.
fn init_canvas(density: &mut EDensity<f64>, core_shapes: &Vec<SimpleRPolygon<db::Coord>>, target_density: f64) {
    debug_assert!(target_density > 0.);
    density.clear();

    // Decompose the core shape into non-overlapping rectangles.
    let rectangles = iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles(
        core_shapes.iter().flat_map(|s| s.edges())
    );

    for r in &rectangles {
        density.draw_charge_density(&r.cast(), -target_density);
    }
}

/// Get simplified pin locations for all cells.
/// For each pin, an average location of its shapes is chosen.
/// They are used do make the global placement aware of the pin locations of macro cells.
fn extract_pin_locations<C: L2NBase>(chip: &C) -> HashMap<C::CellId, HashMap<C::PinId, db::Point<C::Coord>>>
    where C::Coord: FromPrimitive {
    let mut pin_locations = HashMap::new();

    for cell in chip.each_cell() {
        let mut cell_pin_locations = HashMap::new();

        for pin in chip.each_pin(&cell) {
            // Get centers of all pin shapes.
            let pin_locations: Vec<_> = chip.shapes_of_pin(&pin)
                .filter_map(|shape_id| {
                    chip.with_shape(&shape_id, |_layer, shape| {
                        shape.try_bounding_box().map(|bbox| bbox.center())
                    })
                })
                .collect();
            // Compute the average pin location as an approximation if there are multiple pins.
            if !pin_locations.is_empty() {
                let sum: db::Point<C::Coord> = pin_locations.iter().copied().sum();
                let avg = sum / C::Coord::from_usize(pin_locations.len()).unwrap();

                cell_pin_locations.insert(pin, avg);
            }
        }

        pin_locations.insert(cell, cell_pin_locations);
    }

    pin_locations
}

// Prepare data for FFT.
struct OptimizationProblem<'a, C: L2NBase> {
    components: Vec<Component<C>>,
    /// Indices of movable (not fixed) components.
    movable_components: Vec<usize>,
    density: EDensity<f64>,
    net_weights: &'a HashMap<C::NetId, f64>,
    component_indices_by_net: HashMap<C::NetId, Vec<(usize, db::Point<f64>)>>,
}

impl<'a, C: L2NBase> OptimizationProblem<'a, C> {
    /// Compute wire length and gradient.
    /// `positions`: Optimization parameter.
    fn compute_wire_forces(&self, positions: &Vec<db::Vector<f64>>)
                           -> (f64, ndarray::Array1<db::Vector<f64>>, ndarray::Array1<db::Vector<f64>>) {

        // Expand parameter vector to also hold the positions of non-movable components.
        let mut all_positions: Vec<_> = self.components.iter()
            .map(|c| c.position.displacement).collect();
        self.movable_components.iter().zip(positions)
            .for_each(|(&idx, &pos)| all_positions[idx] = pos);

        // Sanity check on array sizes.
        assert_eq!(positions.len(), self.movable_components.len());
        assert_eq!(all_positions.len(), self.components.len());

        let mut net_wl_gradient = vec![Zero::zero(); self.components.len()];
        let mut net_wl_second_order_gradient = vec![Zero::zero(); self.components.len()];

        // Compute wirelength and gradient.
        let wire_length = {

            // Get nets and their weights.
            let nets = self.component_indices_by_net.iter()
                .map(|(net, components)|
                    (self.net_weights.get(net).copied().unwrap_or(1.0), components)
                );

            compute_wirelength_and_gradient(
                nets,
                &self.components,
                &all_positions,
                &mut net_wl_gradient,
                &mut net_wl_second_order_gradient,
            )
        };

        if wire_length.is_nan() {
            log::error!("Wire length is NaN.");
        }

        // Use only forces of movable components.
        {
            // Check that the indices are sorted.
            debug_assert!(self.movable_components.iter()
                              .zip(self.movable_components.iter().skip(1))
                              .all(|(a, b)| a < b), "movable_components must be sorted.");

            for (write_idx, &read_idx) in self.movable_components.iter().enumerate() {
                net_wl_gradient[write_idx] = net_wl_gradient[read_idx];
                net_wl_second_order_gradient[write_idx] = net_wl_second_order_gradient[read_idx];
            };
            net_wl_gradient.truncate(self.movable_components.len());
            net_wl_second_order_gradient.truncate(self.movable_components.len());
        }

        (
            wire_length,
            ndarray::Array1::from_vec(net_wl_gradient),
            ndarray::Array1::from_vec(net_wl_second_order_gradient)
        )
    }

    /// Compute the density and it's gradient and diagonal of the hessian matrix..
    /// `positions`: Optimization parameter.
    fn compute_electric_forces(&mut self, positions: &Vec<db::Vector<f64>>)
                               -> (f64, ndarray::Array1<db::Vector<f64>>, ndarray::Array1<db::Vector<f64>>)
    {
        // Sanity check on array sizes.
        assert_eq!(positions.len(), self.movable_components.len());

        let efield = self.density.compute_efield();

        // dump_array2("/tmp/potential.csv", &potential.map(|c| c.re));
        dump_array2("/tmp/potential_gradient_x.csv", &efield.potential_gradient.map(|c| c.re));
        dump_array2("/tmp/potential_gradient_y.csv", &efield.potential_gradient.map(|c| c.im));


        // Compute the total energy in the potential field.
        // This is a measure for the density mismatch. The goal
        // of the optimization is to bring the energy down to zero.
        let energy: f64 = self.density.charge_distribution.data.iter()
            .map(|p| p.re * p.re)
            .kahan_sum();

        // // Create interpolations.
        // let (potential_interp, potential_gradient_interp) = {
        //     let dim = efield.charge_distribution.dimension;
        //     let (nx, ny) = efield.charge_distribution.get_data_ref().dim();
        //     let (x_min, y_min) = efield.charge_distribution.pixel_center((0, 0)).into();
        //     let (x_max, y_max) = efield.charge_distribution.pixel_center((nx - 1, ny - 1)).into();
        //     let x_axis = ndarray::Array::linspace(x_min, x_max, nx).to_vec();
        //     let y_axis = ndarray::Array::linspace(y_min, y_max, ny).to_vec();
        //
        //
        //     // Create interpolations.
        //     let potential_interp = Interp2D::new(x_axis.clone(), y_axis.clone(), efield.potential.view());
        //     let potential_gradient_interp = Interp2D::new(x_axis, y_axis, efield.potential_gradient.view());
        //     (potential_interp, potential_gradient_interp)
        // };

        // Find size of a pixel.
        // This will determine the integration step size for finding the electric forces.
        // let pixel_size = {
        //     let (w, h) = self.density_bins.pixel_dimension();
        //     w.min(h)
        // };

        // Compute forces on all movable components.
        let mut gradients: Vec<db::Vector<f64>> = vec![Zero::zero(); self.movable_components.len()];
        let mut second_order_gradients: Vec<db::Vector<f64>> = vec![Zero::zero(); self.movable_components.len()];

        for (i, &c_idx) in self.movable_components.iter().enumerate() {
            let c = &self.components[c_idx];
            let mut position = c.position;
            position.displacement = positions[i];
            // Compute positioned shape of the cell.
            let r = c.size.transform(|p| position.transform_point(p));

            // Compute force and force derivatives (f_x/dx and f_y/dy).
            let (force, force_gradient) = {
                efield.compute_force(&r, c.charge_density)
            };

            // Store the force.
            gradients[i] = -force;
            second_order_gradients[i] = force_gradient;

            // { // Compute hessian preconditioning factors.
            //     let gradient = if r.lower_left.x == r.upper_right.x || r.lower_left.y == r.upper_right.y {
            //         // r has zero area. Force is zero.
            //         Zero::zero()
            //     } else {
            //         // Compute [[dx*dx, dx*dy], [dy*dx, dy*dy]]
            //         let hessian = boundary_integral(
            //             &|(x, y)| {
            //                 // TODO
            //                 let g = potential_gradient_interp.eval((x, y));
            //                 g
            //             },
            //             r,
            //             c.charge_density,
            //             pixel_size,
            //         );
            //         // Extract [dx*dx, dy*dy].
            //         db::Vector::new(hessian.x.re, hessian.y.im)
            //
            //     };
            //
            //     // Store the gradient.
            //     forces_gradient[i] = gradient;
            // }
        }

        (energy, ndarray::Array1::from_vec(gradients), ndarray::Array1::from_vec(second_order_gradients))
    }
}

/// Struct for creating electric charge distributions and computing their electric fields.
struct EDensity<F> {
    /// Electric charge distribution.
    charge_distribution: DensityMap<F, Complex<F>>,
    /// Convolution kernel for computing gradient of electric potential (electric field) in frequency domain.
    kernel_potential_gradient: Array2<Complex<F>>,
    /// Convolutional kernel for computing second order gradient of electric potential.
    kernel_potential_second_order_gradient: Array2<Complex<F>>,
}

/// Struct that holds the result of an electric field computation.
struct EField<F> {
    charge_distribution: DensityMap<F, Complex<F>>,
    /// Buffer for holding electric field.
    potential_gradient: Array2<Complex<F>>,
    /// Buffer for holding the second order gradient of the electric potential..
    potential_second_order_gradient: Array2<Complex<F>>,
}

impl<F: Float + Copy + Default + FromPrimitive + NumAssign> EField<F> {
    /// Compute force `F` on a rectangle `r` with a given `charge_density`.
    /// Also compute derivatives `F_x/dx` and `F_y/dy`.
    fn compute_force(&self, r: &db::Rect<F>, charge_density: F) -> (db::Vector<F>, db::Vector<F>) {
        // Compute force and force derivatives (f_x/dx and f_y/dy).

        assert!(self.charge_distribution.dimension.contains_rectangle(r), "Rectangle is not inside boundary of charge distribution.");

        // Find indices of pixels that interact with the corners of the rectangle `r`.
        let (xstart, ystart) = self.charge_distribution.coordinates_to_indices(r.lower_left);
        let (xend, yend) = self.charge_distribution.coordinates_to_indices(r.upper_right);

        let mut force = Zero::zero();
        let mut force_gradient = Zero::zero();

        let _2 = F::from_usize(2).unwrap();

        let pixel_area = self.charge_distribution.bin_area();

        // Loop over all pixels that interact with the rectangle `r`.
        for x in xstart..xend + 1 {
            for y in ystart..yend + 1 {
                // Compute how much r overlaps with the current pixel.
                let overlap_area = if x == xstart || x == xend || y == ystart || y == yend {
                    // Corner and edge cases with partial overlap.
                    // Compute increment by the overlap of the pixel shape and the rectangle.
                    let pixel_shape = self.charge_distribution.get_bin_shape((x, y));

                    let overlap_area = pixel_shape.intersection(&r)
                        .map(|r| r.area_doubled_oriented() / _2)
                        // .expect("Pixel must intersect with rectangle!");
                        .unwrap_or(F::zero());
                    overlap_area
                } else {
                    // Pixel is fully covered by the rectangle r.
                    pixel_area
                };

                let f = self.potential_gradient[[x, y]] * overlap_area * charge_density;
                let f_gradient = self.potential_second_order_gradient[[x, y]] * overlap_area * charge_density;


                force += db::Vector::new(f.re, f.im);
                force_gradient += db::Vector::new(f_gradient.re, f_gradient.im);
            }
        }
        (force, force_gradient)
    }

    // /// Compute the force and force gradient on a point charge at location `p`.
    // fn compute_force_on_point(&self, p: &db::Point<F>, charge: F) -> (db::Vector<F>, db::Vector<F>) {
    //     // Compute force and force derivatives (f_x/dx and f_y/dy).
    //
    //     assert!(self.charge_distribution.dimension.contains_point(p), "Point is not inside boundary of charge distribution.");
    //
    //     let (x, y) = p.into();
    //
    //     let f = self.potential_gradient[[x, y]] * charge;
    //     let f_gradient = self.potential_second_order_gradient[[x, y]] * charge;
    //
    //     let force = db::Vector::new(f.re, f.im);
    //     let force_gradient = db::Vector::new(f_gradient.re, f_gradient.im);
    //
    //     (force, force_gradient)
    // }
}

impl<F: Float + Copy + Default + FromPrimitive + FFTFloat + NumAssign> EDensity<F> {
    /// Allocate space and pre-compute convolutional kernels in spacial domain.
    fn new(r: db::Rect<F>, (w, h): (usize, usize)) -> Self {
        let density_bins = DensityMap::new(r, (w, h));

        // Prepare convolutional kernel for computing the potential.
        let potential_kernel_fft = {
            let _2 = F::from_usize(2).unwrap();
            let mut potential_kernel = electrostatic_potential_kernel(
                density_bins.get_data_ref().dim(),
                (density_bins.dimension.width(), density_bins.dimension.height()),
                (density_bins.dimension.width() / _2, density_bins.dimension.height() / _2),
            );
            //
            // {
            //     let potential_kernel_real = potential_kernel.map(|c| c.re);
            //     dump_array2("/tmp/potential_kernel.csv", &potential_kernel_real);
            // }

            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut potential_kernel);
            potential_kernel
        };

        // Prepare convolutional kernel for computing the gradient of the potential (e-field).
        let potential_gradient_kernel_fft = {
            let mut gradient_kernel = gradient_kernel(
                density_bins.get_data_ref().dim(),
                (density_bins.dimension.width(), density_bins.dimension.height()),
            );
            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut gradient_kernel);

            gradient_kernel * &potential_kernel_fft
        };

        // Prepare convolutional kernel for computing the second order gradient of the potential.
        // Used for computing the hessian pre-conditioning factors.
        let potential_second_order_gradient_kernel_fft = {
            let mut snd_order_gradient_kernel = second_order_gradient_kernel(
                density_bins.get_data_ref().dim(),
                (density_bins.dimension.width(), density_bins.dimension.height()),
            );
            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut snd_order_gradient_kernel);

            snd_order_gradient_kernel * &potential_kernel_fft
        };
        Self {
            charge_distribution: density_bins,
            kernel_potential_gradient: potential_gradient_kernel_fft,
            kernel_potential_second_order_gradient: potential_second_order_gradient_kernel_fft,
        }
    }


    /// Set the charge distribution to zero.
    fn clear(&mut self) {
        self.charge_distribution.data.iter_mut().for_each(|v| *v = Zero::zero());
    }

    /// Add a charge density of rectangular shape.
    fn draw_charge_density(&mut self, r: &db::Rect<F>, charge_density: F) {
        assert!(self.charge_distribution.dimension.contains_rectangle(r));
        self.charge_distribution.draw_rect(r, Complex::new(charge_density, F::zero()));
    }

    /// Compute the electric field and second order gradient of the potential.
    fn compute_efield(&self) -> EField<F> {
        // FFT convolve.
        // TODO: Do with less allocations? Maybe does not matter.
        let fft = fft2d::fft2(&self.charge_distribution.data);
        // let potential = fft2d::ifft2(&(&fft * &self.potential_kernel_fft));

        // Compute the gradient and second order gradient in parallel.
        let kernels = vec![&self.kernel_potential_gradient, &self.kernel_potential_second_order_gradient];

        let mut convolutions: Vec<_> = kernels.into_par_iter() // Parallel iterator.
            .map(|kernel| fft2d::ifft2(&(&fft * kernel)))
            .collect();

        let potential_second_order_gradient = convolutions.pop().unwrap();
        let potential_gradient = convolutions.pop().unwrap();


        EField {
            charge_distribution: self.charge_distribution.clone(),
            potential_gradient,
            potential_second_order_gradient,
        }
    }
}

#[test]
fn test_compute_efield_single_particle() {
    let mut density = EDensity::new(db::Rect::new((0., 0.), (10., 10.)), (10, 10));

    let particle = db::Rect::new((5., 5.), (6., 6.));
    density.draw_charge_density(
        &particle,
        1.,
    );

    let efield = density.compute_efield();

    let (force, _force_derivative) = efield.compute_force(&particle, 2.);

    let tol = 1e-6;
    // dbg!(force_derivative);
    assert!(force.x.abs() < tol);
    assert!(force.y.abs() < tol);
    // assert!(force_derivative.x.abs() < tol);
    // assert!(force_derivative.y.abs() < tol);

    {
        let particle2 = db::Rect::new((6., 5.), (7., 6.));
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);
        dbg!(force);
        dbg!(force_derivative);
        assert!(force.x > 0.0);
        assert!(force.y.abs() < tol);
    }
    // {
    //     let d = 1e-8;
    //     let neg = db::Rect::new((6.-d, 5.), (6.-d, 6.));
    //     let pos = db::Rect::new((6.+d, 5.), (6.+d, 6.));
    //     let (force_neg, _) = efield.compute_force(&neg, 1.);
    //     let (force_pos, _) = efield.compute_force(&pos, 1.);
    //     let derivative = (force_pos - force_neg) / (d*2.);
    //     dbg!(derivative);
    // }
    // {
    //     let d = 1e-8;
    //     let neg = db::Rect::new((6., 5.-d), (7., 6.-d));
    //     let pos = db::Rect::new((6., 5.+d), (7., 6.+d));
    //     let (force_neg, _) = efield.compute_force(&neg, 1.);
    //     let (force_pos, _) = efield.compute_force(&pos, 1.);
    //     let derivative = (force_pos - force_neg) / (d*2.);
    //     dbg!(derivative);
    // }
    {
        let particle2 = db::Rect::new((4., 5.), (5., 6.));
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);
        dbg!(force);
        dbg!(force_derivative);
        assert!(force.x < 0.0);
        assert!(force.y.abs() < tol);
    }
    {
        let particle2 = db::Rect::new((5., 6.), (6., 7.));
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);
        dbg!(force);
        dbg!(force_derivative);
        assert!(force.x.abs() < tol);
        assert!(force.y > 0.);
    }
    {
        let particle2 = db::Rect::new((5., 4.), (6., 5.));
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);
        dbg!(force);
        dbg!(force_derivative);
        assert!(force.x.abs() < tol);
        assert!(force.y < 0.);
    }
}
