// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Models for differentiable wire lengths.
//! The half-perimeter wire length (HPWL) of a net is defined as the sum of with and height of the bounding
//! box around a net. This value is not differentiable.
//! Hence two approximations are used:
//! * log-sum-exp (LSE)
//! * weighted-average (WA)
//!
//! # References
//! * ePlace-MS, formulas (3), (24), (25) and (4), (26): <https://cseweb.ucsd.edu/~jlu/papers/eplace-ms-tcad14/paper.pdf>

use libreda_pnr::db;
use num_traits::Zero;
use crate::kahan_sum::KahanAccumulator;

/// HPWL (half-perimeter wire length) approximation based on the
/// approximation of `min` and `max` functions with exponential
/// and logarithms. This has the advantage to be derivable.
///
/// The error compared to the true HPWL is expected to be in the order of `gamma`.
///
/// A small `gamma` leads to more accurate results but also quickly yields `NaN`s and
/// makes the function less smooth.
///
/// Make sure that `(p.x/gamma).exp()` does not overflow for both `x` and `y` coordinates of the points.
///
/// Returns `(wire length x, wire length y)`.
pub fn log_sum_exp_wire_length<I: Iterator<Item=db::Point<f64>>>(points: I, gamma: f64) -> (f64, f64) {
    assert_ne!(gamma, 0., "gamma can't be zero!");

    let mut acc_pos = KahanAccumulator::new();
    let mut acc_neg = KahanAccumulator::new();

    let mut is_defined = false;
    points.for_each(|p| {
        acc_pos.update(db::Vector::new((p.x / gamma).exp(), (p.y / gamma).exp()));
        acc_neg.update(db::Vector::new((-p.x / gamma).exp(), (-p.y / gamma).exp()));

        is_defined = true;
    });

    if is_defined {
        let sum_pos = acc_pos.sum();
        let sum_neg = acc_neg.sum();
        let wl_x = gamma * (sum_pos.x.ln() + sum_neg.x.ln());
        let wl_y = gamma * (sum_pos.y.ln() + sum_neg.y.ln());

        (wl_x, wl_y)
    } else {
        // No points given -> wire length is zero.
        (Zero::zero(), Zero::zero())
    }
}

#[test]
fn test_log_sum_exp_wire_length() {
    {
        let single_point: Vec<db::Point<_>> = vec![(10., 100.).into()];
        assert_eq!(log_sum_exp_wire_length(single_point.iter().copied(), 1.), (0., 0.));
    }

    {
        let gamma = 1.0;
        let two_points: Vec<db::Point<_>> = vec![(100., 100.).into(), (110., 120.).into()];
        let (lx, ly) = log_sum_exp_wire_length(two_points.iter().copied(), gamma);

        assert!((lx - 10.).abs() < 1e-3);
        assert!((ly - 20.).abs() < 1e-3);
    }

    {
        let gamma = 10.0;
        let many_points: Vec<db::Point<_>> = vec![
            (-100., -200.).into(),
            (100., 200.).into(),
            (90., 190.).into(),
            (0., 0.).into(),
            (-99., 0.).into()];
        let (lx, ly) = log_sum_exp_wire_length(many_points.iter().copied(), gamma);
        dbg!(lx, ly);
        assert!((lx - 200.).abs() < gamma);
        assert!((ly - 400.).abs() < gamma);
    }
}

/// Compute the gradient of the log-sum-exp (LSE) wire length model.
/// The gradient value for each of the points is pushed into the `result` vector.
pub fn log_sum_exp_wire_length_gradient<I: Clone + Iterator<Item=db::Point<f64>>>(
    points: I,
    gamma: f64,
    result: &mut Vec<db::Vector<f64>>,
) {
    assert_ne!(gamma, 0., "gamma can't be zero!");

    let mut acc_pos = KahanAccumulator::new();
    let mut acc_neg = KahanAccumulator::new();

    // Compute the sums.
    points.clone().for_each(|p| {
        let x = (p.x / gamma).exp();
        let y = (p.y / gamma).exp();
        let x_neg = (-p.x / gamma).exp();
        let y_neg = (-p.y / gamma).exp();

        // Compute the sum.
        acc_pos.update(db::Vector::new(x, y));
        acc_neg.update(db::Vector::new(x_neg, y_neg));
    });

    let sum_pos = acc_pos.sum();
    let sum_neg = acc_neg.sum();

    points.for_each(|p| {
        let x = (p.x / gamma).exp();
        let y = (p.y / gamma).exp();
        let x_neg = (-p.x / gamma).exp();
        let y_neg = (-p.y / gamma).exp();

        let pos = db::Vector::new(x, y);
        let neg = db::Vector::new(x_neg, y_neg);

        result.push(
            db::Vector::new(
                pos.x / sum_pos.x - neg.x / sum_neg.x,
                pos.y / sum_pos.y - neg.y / sum_neg.y,
            )
        )
    });
}


#[test]
fn test_log_sum_exp_wire_length_gradient() {
    {
        // Single point. Gradient should be zero
        // (moving a single point does not change the size of the bounding box).
        let gamma = 1.0;
        let points: Vec<db::Point<_>> = vec![(10., 100.).into()];
        let mut gradient = Vec::new();
        log_sum_exp_wire_length_gradient(points.iter().copied(),
                                         gamma,
                                         &mut gradient,
        );
        assert_eq!(gradient.len(), points.len());
        assert_eq!(gradient, vec![db::Vector::zero()]);
    }

    {
        // Three points.
        let gamma = 1.0;
        let points: Vec<db::Point<_>> = vec![
            (0., 0.).into(),  // Lower left corner.
            (100., 100.).into(), // Upper right corner.
            (50., 50.).into() // Somewhere in the middle.
        ];
        let mut gradient = Vec::new();
        log_sum_exp_wire_length_gradient(points.iter().copied(),
                                         gamma,
                                         &mut gradient,
        );
        assert_eq!(gradient.len(), points.len());

        assert!((gradient[0] - (-1., -1.).into()).norm2() < gamma);
        assert!((gradient[1] - (1., 1.).into()).norm2() < gamma);
        // The node in the middle should have a low gradient.
        assert!(gradient[2].norm2() < gamma);
    }
}

/// Compute the second order gradient of `log_sum_exp_wire_length`.
/// See (25) in [1].
///
/// * `gamma`: Must be larger than `0.0`.
pub fn log_sum_exp_wire_length_second_order_gradient<I: Clone + Iterator<Item=db::Point<f64>>>(
    points: I,
    gamma: f64,
    result: &mut Vec<db::Vector<f64>>,
) {
    assert_ne!(gamma, 0., "gamma can't be zero!");

    let mut sum_pos: db::Vector<f64> = Zero::zero();
    let mut sum_neg: db::Vector<f64> = Zero::zero();

    // Compute the sums.
    points.clone().for_each(|p| {
        let x = (p.x / gamma).exp();
        let y = (p.y / gamma).exp();
        let x_neg = (-p.x / gamma).exp();
        let y_neg = (-p.y / gamma).exp();

        // Compute the sum.
        sum_pos += db::Vector::new(x, y);
        sum_neg += db::Vector::new(x_neg, y_neg);
    });

    let sum_pos_squared = db::Vector::new(sum_pos.x * sum_pos.x, sum_pos.y * sum_pos.y);
    let sum_neg_squared =  db::Vector::new(sum_neg.x * sum_neg.x, sum_neg.y * sum_neg.y);

    points.for_each(|p| {
        let x = (p.x / gamma).exp();
        let y = (p.y / gamma).exp();
        let x_neg = (-p.x / gamma).exp();
        let y_neg = (-p.y / gamma).exp();

        result.push(
            db::Vector::new(
                x*(sum_pos.x-x)/sum_pos_squared.x + x_neg*(sum_neg.x-x_neg)/sum_neg_squared.x,
                y*(sum_pos.y-y)/sum_pos_squared.y + y_neg*(sum_neg.y-y_neg)/sum_neg_squared.y,
            ) / gamma
        )
    });
}

#[test]
fn test_log_sum_exp_wire_length_second_order_gradient() {
    {
        // Single point. Second-order gradient should be zero
        // (moving a single point does not change the size of the bounding box).
        let gamma = 1.0;
        let points: Vec<db::Point<_>> = vec![(10., 100.).into()];
        let mut second_gradient = Vec::new();
        log_sum_exp_wire_length_second_order_gradient(points.iter().copied(),
                                                      gamma,
                                                      &mut second_gradient,
        );
        assert_eq!(second_gradient.len(), points.len());
        assert_eq!(second_gradient, vec![db::Vector::zero()]);
    }

    {
        // Three points.
        let gamma = 1.0;
        let points: Vec<db::Point<_>> = vec![
            (0., 0.).into(),  // Lower left corner.
            (100., 100.).into(), // Upper right corner.
            (100., 50.).into() // Somewhere in the middle.
        ];
        let mut second_gradient = Vec::new();
        log_sum_exp_wire_length_second_order_gradient(points.iter().copied(),
                                                      gamma,
                                                      &mut second_gradient,
        );
        dbg!(&second_gradient);
        assert_eq!(second_gradient.len(), points.len());

        let tol = 1e-6;
        // Lower left corner does not change the gradient.
        assert!(second_gradient[0].x < tol);
        assert!(second_gradient[0].y < tol);

        // The other two are on the same x-coordinate, so moving them along the
        // x-axis changes the gradient (second_gradient > 0).
        assert!(second_gradient[1].x > tol);
        assert!(second_gradient[1].y < tol);

        assert!(second_gradient[2].x > tol);
        assert!(second_gradient[2].y < tol);

    }
}

/// HPWL (half-perimeter wire length) approximation based on the
/// approximation of `min` and `max` functions as weighted averages.
/// This has the advantage to be derivable.
///
/// The error compared to the true HPWL is expected to be in the order of `gamma`.
///
/// A small `gamma` leads to more accurate results but also quickly yields `NaN`s and
/// makes the function less smooth.
///
/// Returns `(wire length x, wire length y)`.
#[allow(unused)]
pub fn weighted_average_wire_length<I: Iterator<Item=db::Point<f64>>>(points: I, gamma: f64) -> (f64, f64) {
    debug_assert!(gamma != 0., "gamma can't be zero!");

    let mut sum_pos_weighted: db::Vector<f64> = Zero::zero();
    let mut sum_neg_weighted: db::Vector<f64> = Zero::zero();
    let mut sum_pos: db::Vector<f64> = Zero::zero();
    let mut sum_neg: db::Vector<f64> = Zero::zero();

    points.for_each(|p| {
        let x_exp = (p.x / gamma).exp();
        let y_exp = (p.y / gamma).exp();
        let x_neg_exp = (-p.x / gamma).exp();
        let y_neg_exp = (-p.y / gamma).exp();

        let pos = db::Vector::new(x_exp, y_exp);
        let neg = db::Vector::new(x_neg_exp, y_neg_exp);

        // Compute the sum.
        sum_pos += pos;
        sum_neg += neg;

        sum_pos_weighted.x += p.x * x_exp;
        sum_pos_weighted.y += p.y * y_exp;
        sum_neg_weighted.x += p.x * x_neg_exp;
        sum_neg_weighted.y += p.y * y_neg_exp;
    });

    let wl_x = sum_pos_weighted.x / sum_pos.x - sum_neg_weighted.x / sum_neg.x;
    let wl_y = sum_pos_weighted.y / sum_pos.y - sum_neg_weighted.y / sum_neg.y;

    (wl_x, wl_y)
}


#[test]
fn test_weighted_average_wire_length() {
    {
        let single_point: Vec<db::Point<_>> = vec![(10., 100.).into()];
        assert_eq!(weighted_average_wire_length(single_point.iter().copied(), 1.), (0., 0.));
    }

    {
        let gamma = 1.0;
        let two_points: Vec<db::Point<_>> = vec![(100., 100.).into(), (110., 120.).into()];
        let (lx, ly) = weighted_average_wire_length(two_points.iter().copied(), gamma);

        assert!((lx - 10.).abs() < 1e-3);
        assert!((ly - 20.).abs() < 1e-3);
    }

    {
        let gamma = 10.0;
        let many_points: Vec<db::Point<_>> = vec![
            (-100., -200.).into(),
            (100., 200.).into(),
            (90., 190.).into(),
            (0., 0.).into(),
            (-99., 0.).into()];
        let (lx, ly) = weighted_average_wire_length(many_points.iter().copied(), gamma);
        dbg!(lx, ly);
        assert!((lx - 200.).abs() < gamma);
        assert!((ly - 400.).abs() < gamma);
    }
}