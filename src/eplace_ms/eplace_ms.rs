// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! `EPlaceMS` combines the single placement steps (global placement, macro legalization,
//! standard-cell-only placement, standard-cell legalization) into a single placement step.

// libreda
use libreda_pnr::db;
use db::traits::*;
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};

// crates.io
use log;

// stdlib
use std::collections::{HashMap, HashSet};
use itertools::Itertools;
use crate::eplace_ms::MixedSizeGlobalPlacer;
use crate::eplace_ms::macro_legalizer::AnnealingMacroLegalizer;
use libreda_pnr::place::placement_problem::{PlacementProblem, PlacementStatus};
use libreda_pnr::place::placement_problem_overlay::PlacementProblemOverlay;

/// Combination of global placer, macro legalization and standard cell legalization.
pub struct EPlaceMS {
    /// Target placement density. Must be larger than zero and should be smaller than one.
    target_density: f64,
    /// Maximal number of placement iterations.
    max_iter: usize,
}


impl EPlaceMS {
    /// * `target_density`: Target placement density. Must be larger than zero and should be smaller than one.
    pub fn new(target_density: f64) -> Self {
        assert!(target_density > 0., "Target density must be larger than 0.");
        if target_density > 1. {
            // Warn about high target density.
            log::warn!("Target density is larger than 1: {}", target_density);
        }
        Self {
            target_density,
            max_iter: 200,
        }
    }

    /// Set maximum number of iterations.
    pub fn max_iter(&mut self, max_iter: usize) {
        self.max_iter = max_iter;
    }
}


/// Find size of smallest cell. Based on this size we will detect macros.
/// A macro is considered a cell which does not fit in a std-cell row.
fn find_smallest_cell_size<C: db::L2NBase<Coord=db::Coord>>(
    placement_problem: &dyn PlacementProblem<C>
) -> db::Rect<C::Coord> {

    let top_ref = placement_problem.fused_layout_netlist()
        .cell_ref(&placement_problem.top_cell());

    log::debug!("Find smallest cell size.");
    let smallest_cell_size: db::Rect<_> =
        top_ref.each_cell_dependency()
            .flat_map(|cell| placement_problem.cell_outline(&cell.id()))
            .min_by_key(|r| r.width() as usize * r.height() as usize)
            .expect("No cell outlines given!");
    smallest_cell_size
}


/// Detect macro cells.
fn find_macro_cells<C: db::L2NBase<Coord=db::Coord>>(
    placement_problem: &dyn PlacementProblem<C>
) -> Vec<C::CellId>{
    // Find size of smallest cell. Based on this size we will detect macros.
    // A macro is considered a cell which does not fit in a std-cell row.
    log::debug!("Find smallest cell size.");
    let smallest_cell_size: db::Rect<_> = find_smallest_cell_size(placement_problem);

    let top_ref = placement_problem.fused_layout_netlist()
        .cell_ref(&placement_problem.top_cell());

    // Detect macro cells.
    log::debug!("Detect macro cells.");
    let macro_cells: Vec<_> = {
        let mut macro_cells: Vec<_> = top_ref.each_cell_dependency()
            .filter_map(|cell| {
                // Select cells which have a defined outline which is larger than the standard cell height.
                if let Some(outline) = placement_problem.cell_outline(&cell.id()) {
                    if outline.width() > smallest_cell_size.width() && outline.height() > smallest_cell_size.height() {
                        Some(cell.id())
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();

        // Sort by name.
        macro_cells.sort_by_key(|c| placement_problem.fused_layout_netlist().cell_name(c));

        macro_cells
    };

    macro_cells
}

impl<C> MixedSizePlacer<C> for EPlaceMS
    where C: db::L2NBase<Coord=db::Coord> {
    fn name(&self) -> &str {
        "ePlace_mGP"
    }

    fn find_cell_positions_impl(&self, placement_problem: &dyn PlacementProblem<C>)
                                -> Result<HashMap<C::CellInstId, db::SimpleTransform<db::Coord>>, PlacementError> {

        let chip = placement_problem.fused_layout_netlist();

        // Create mutable overlay.
        let mut placement_problem = PlacementProblemOverlay::new(placement_problem);

        let top_cell = placement_problem.top_cell();

        let top_ref = chip.cell_ref(&top_cell);

        // Find size of smallest cell. Based on this size we will detect macros.
        // A macro is considered a cell which does not fit in a std-cell row.
        log::debug!("Find smallest cell size.");
        let smallest_cell_size: db::Rect<_> = find_smallest_cell_size(&placement_problem);

        log::info!("Smallest cell size: {}x{}", smallest_cell_size.width(), smallest_cell_size.height());

        // Detect macro cells.
        log::debug!("Detect macro cells.");
        let macro_cells = find_macro_cells(&placement_problem);
        log::info!("Macro cells: {}", macro_cells.iter().map(|c| chip.cell_name(c)).join(" ,"));

        log::debug!("Find macro instances.");
        let macro_instances: HashSet<_> = chip.each_cell_instance(&top_cell)
            .filter(|inst| {
                let template_cell_id = chip.template_cell(&inst);
                macro_cells.contains(&template_cell_id)
            })
            .collect();
        log::info!("Number of macro instances: {}", macro_instances.len());
        { // Print statistics.
            let stdcell_instances = chip.each_cell_instance(&top_cell)
                .filter(|inst| {
                    let template_cell_id = chip.template_cell(&inst);
                    !macro_cells.contains(&template_cell_id)
                });
            log::info!("Number of standard cell instances: {}", stdcell_instances.count());
        }

        // Quick global placer for mixed-size placement. Works on lower resolution to be faster.
        let mut global_placer1 = MixedSizeGlobalPlacer::new(self.target_density);
        global_placer1.max_iter(self.max_iter);
        global_placer1.pixel_per_cell(0.25);

        // Placer for standard-cells only. Used after macro legalization.
        let mut global_placer2 = MixedSizeGlobalPlacer::new(self.target_density);
        global_placer2.max_iter(self.max_iter);
        global_placer2.pixel_per_cell(1.0);

        // Global placement.
        let global_positions = {
            log::info!("Run global placement.");

            // Run global placement algorithm.
            let positions = global_placer1.find_cell_positions_impl(
                &placement_problem
            );

            if positions.is_err() {
                log::warn!("Global placement terminated with error.");
            } else {
                log::info!("Global placement done.");
            }

            positions?
        };

        // Update current positions.
        for (id, pos) in global_positions {
            placement_problem.initial_positions.insert(id, pos);
        }

        // Macro legalization.
        let legalized_macro_positions = {
            log::info!("Run macro legalization.");

            let legalizer = AnnealingMacroLegalizer {
                max_iter: 8000
            };

            let mut macro_legalization_problem = PlacementProblemOverlay::new(&placement_problem);

            // Mark non-macro cells to be ignored.
            for inst in top_ref.each_cell_instance() {
                let inst_id = inst.id();
                if !macro_instances.contains(&inst_id) {
                    macro_legalization_problem.placement_status.insert(inst_id, PlacementStatus::Ignore);
                };
            }

            // Do legalization.
            let legal_macro_positions = legalizer.find_cell_positions_impl(
                &macro_legalization_problem
            );

            if let Err(err) = &legal_macro_positions {
                log::error!("Macro legalization terminated with error: {:?}", err);
            } else {
                log::info!("Macro legalization done.");
            }

            legal_macro_positions?
        };

        // Update current positions.
        for (id, pos) in legalized_macro_positions {
            placement_problem.initial_positions.insert(id, pos);
        }

        // Global-placement of standard-cells with fixed macros.
        let placed_standard_cells = {
            log::info!("Run global placement with fixed macros.");

            let mut stdcell_placement_problem = PlacementProblemOverlay::new(&placement_problem);

            // Treat all macro-instances as fixed.
            // Update the placement status.
            for inst in &macro_instances {
                stdcell_placement_problem.placement_status.insert(inst.clone(), PlacementStatus::Fixed);
            }

            let positions = global_placer2.find_cell_positions_impl(
                &stdcell_placement_problem
            );

            if positions.is_err() {
                log::warn!("Global placement with fixed macros terminated with error.");
            } else {
                log::info!("Global placement with fixed macros done.");
            }

            positions?
        };

        // Update current positions.
        for (id, pos) in placed_standard_cells {
            placement_problem.initial_positions.insert(id, pos);
        }

        // // Standard-cell legalization.
        // let legalized_standard_cells  = {
        //
        // };
        //
        // // Update current positions.
        // for (id, pos) in legalized_standard_cells {
        //     current_positions.insert(id, pos);
        // }

        log::info!("Done");
        Ok(placement_problem.initial_positions)
    }
}
