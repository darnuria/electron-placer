// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Macro-legalizer based on simulated annealing.

use log;
use rand::prelude::*;
use rand_pcg::Pcg64;
use num_traits::{Bounded, FromPrimitive, ToPrimitive};

use libreda_pnr::db;
use db::MapPointwise;
use db::CoordinateType;
use db::traits::*;
use std::collections::HashMap;
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};
use libreda_pnr::place::placement_problem::{PlacementProblem, PlacementStatus};
use libreda_pnr::db::SimpleTransform;

/// Legalizer for macro blocks based on a 'simulated annealing' optimization algorithm.
/// This implementation is similar to 'mLG' described here: <https://cseweb.ucsd.edu/~jlu/papers/eplace-ms-tcad14/paper.pdf>
pub struct AnnealingMacroLegalizer {
    /// Maximal number of optimization iterations. `0` means unlimited.
    pub max_iter: usize,
}

impl<L: db::L2NBase<Coord=db::Coord>> MixedSizePlacer<L> for AnnealingMacroLegalizer {
    fn name(&self) -> &str {
        "AnnealingMacroLegalizer"
    }


    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<L>,
    ) -> Result<HashMap<L::CellInstId, SimpleTransform<L::Coord>>, PlacementError> {
        // Shortcut to layout/netlist structure.
        let chip = placement_problem.fused_layout_netlist();
        let top_cell = placement_problem.top_cell();
        let top = chip.cell_ref(&top_cell);

        let mut macro_ids = Vec::new(); // List of cell ids.

        // Create simplified representation of cells.
        let mut macros: Vec<MacroCell<L::Coord>> = top.each_cell_instance()
            .filter(|inst| placement_problem.placement_status(&inst.id()) != PlacementStatus::Ignore)
            .map(|inst| {

                let outline = placement_problem.cell_outline(&inst.template_id())
                    .expect("Cell outline not found.");

                let pos = placement_problem.initial_position(&inst.id());

                let is_fixed = placement_problem.placement_status(&inst.id()) == PlacementStatus::Fixed;

                macro_ids.push(inst.id());

                MacroCell {
                    size: outline,
                    original_position: pos,
                    current_position: pos,
                    fixed: is_fixed,
                }
            })
            .collect();

        let positions = legalize_macros(
            &placement_problem.placement_region(),
            &mut macros,
            self.max_iter,
        );

        // TODO: Proper error handling. Return a Result.
        let positions = positions.expect("Legalization failed.");

        let result = macro_ids.into_iter()
            .zip(positions)
            .collect();

        Ok(result)
    }
}

/// Find displacements for all macros such that
/// * the macro blocks don't overlap
/// * the macro blocks are inside the placement region
/// and minimize the weighted sum of perturbations and overlap with the soft-block area.
fn legalize_macros<T>(
    placement_regions: &Vec<db::SimpleRPolygon<T>>,
    macros: &mut Vec<MacroCell<T>>,
    max_iter: usize) -> Option<Vec<db::SimpleTransform<T>>>
    where T: CoordinateType + Ord + Bounded + FromPrimitive + ToPrimitive {
    let seed = 42;
    let mut rng = Pcg64::new(seed, 0xa02bdbf7bb3c0a7ac28fa16a64abf96);

    let num_macros = macros.len();

    log::info!("Number of macros: {}", num_macros);
    log::info!("Max. iterations: {}", max_iter);

    // Update original positions.
    for m in macros.iter_mut() {
        m.original_position = m.current_position;
    }

    // Cost of previous iteration.
    let mut prev_cost = cost_function(placement_regions, &macros, 100).to_f64().unwrap();
    let mut iter_count = 0;
    loop {
        { // Check stopping criteria.
            // Finish when overlap is zero.
            {
                let overlap = macro_overlap(&macros);
                let outside_placement_region = macro_placement_region_overlap(placement_regions, &macros);
                if overlap.is_zero() && outside_placement_region.is_zero() {
                    log::info!("Macro overlap is zero.");
                    break;
                }
            }
        }

        // Choose initial temperature such that a cost increase of 3% (0.03) is accepted with a probability 0.5.
        let p = 0.03;
        let temperature = p * prev_cost / (1. / 0.5_f64).ln();
        assert!(((-p * prev_cost / temperature).exp() - 0.5).abs() < 1e-6);
        assert!((-0.02 * prev_cost / temperature).exp() > 0.5);

        let cost = loop { // Loop until a perturbation is accepted.

            // Abort when maximum number of iterations is reached.
            if iter_count == max_iter {
                log::warn!("No solution found within maximum number of iterations ({}).", max_iter);
                return None;
            }
            iter_count += 1;

            // Pick a random non-fixed macro.
            let current_idx = rng.gen_range(0..num_macros);
            let fixed = macros[current_idx].fixed;
            if !fixed {
                let m = &mut macros[current_idx];
                let (w, h) = (m.size.width().to_isize().unwrap(), m.size.height().to_isize().unwrap());

                // Perturb the position.
                let perturbation: db::Vector<T> = db::Vector::new(
                    T::from_isize(rng.gen_range(0..w) - w / 2).unwrap(),
                    T::from_isize(rng.gen_range(0..h) - h / 2).unwrap(),
                );

                // Remember previous location (used for reset when new perturbation is not accepted).
                let prev_location = m.current_position;
                // Update current location.
                m.current_position.displacement = prev_location.displacement + perturbation;
                let cost = cost_function(placement_regions, &macros, 100);
                let cost = cost.to_f64().unwrap();
                let delta_cost = cost - prev_cost;

                let tau = rng.gen_range(0.0..1.0); // Acceptance threshold.
                // println!("delta_cost = {}", delta_cost);
                // println!("tau = {}", tau);
                // println!("temperature = {}", temperature);
                // println!("(-delta_cost / temperature).exp() = {}", (-delta_cost / temperature).exp());
                let accept = tau < (-delta_cost / temperature).exp();
                // println!("accept = {}", accept);
                // println!();

                if !accept {
                    // Undo the current perturbation.
                    macros[current_idx].current_position = prev_location;
                } else {
                    // Accepted perturbation. Leave the inner loop.
                    break cost;
                }
            }
        };
        // log::info!("cost = {}", cost);
        // println!("cost = {}", cost);

        prev_cost = cost;
    }

    // Return the displacements.
    Some(
        macros.iter()
            .map(|m| m.current_position)
            .collect()
    )
}

/// Cost function for the simulated-annealing optimization.
/// Cost is computed as a weighted sum of overlapping macro area,
/// macro area outside the placement region, overlap of macros with soft-block,
/// total perturbation from the original locations (TODO: take HPWL into cost function).
fn cost_function<T>(placement_regions: &Vec<db::SimpleRPolygon<T>>,
                    // soft_block: &Vec<db::SimpleRPolygon<T>>,
                    macros: &Vec<MacroCell<T>>,
                    overlap_weight: usize,
) -> T
    where T: CoordinateType + Ord + Bounded + FromPrimitive {
    let overlap = macro_overlap(macros);
    let outside_placement_region = macro_placement_region_overlap(placement_regions, macros);

    let displacement_sum = macros.iter()
        .map(|m| {
            let diff = m.current_position.displacement - m.original_position.displacement;
            diff.norm1()
        })
        .fold(T::zero(), |a, b| a + b);

    let macro_overlap_weight = T::from_usize(overlap_weight).unwrap();

    macro_overlap_weight * (overlap + outside_placement_region) + displacement_sum
}

/// Representation of a macro cell.
struct MacroCell<T> {
    /// Outline of the macro.
    size: db::Rect<T>,
    /// Original position of the macro. Used to minimize the perturbations.
    original_position: db::SimpleTransform<T>,
    /// Current position of the macro.
    current_position: db::SimpleTransform<T>,
    /// Location is fixed and will not be changed during optimization.
    fixed: bool,
}

impl<T: CoordinateType> MacroCell<T> {
    /// Get the shape moved to the current position.
    fn absolute_shape(&self) -> db::Rect<T> {
        self.size.transform(|p| self.current_position.transform_point(p))
    }
}

/// Compute the total macro area which is overlapping with other macros.
/// TODO: Naive implementation has O(n^2). A scanline approach would be faster.
fn macro_overlap<T: CoordinateType>(macros: &Vec<MacroCell<T>>) -> T {
    let mut overlap = T::zero();

    for (i, m1) in macros.iter().enumerate() {
        let r1 = m1.absolute_shape();
        for (j, m2) in macros.iter().enumerate() {
            if i != j {
                let r2 = m2.absolute_shape();
                let intersection = r1.intersection(&r2);
                overlap = overlap + intersection.map(|r| r.width() * r.height()).unwrap_or(T::zero());
            }
        }
    }

    overlap
}

#[test]
fn test_macro_overlap() {
    let mut macros = vec![
        MacroCell {
            size: db::Rect::new((0, 0), (10, 10)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
        MacroCell {
            size: db::Rect::new((0, 0), (10, 10)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((10, 10)),
            fixed: false,
        }
    ];

    assert_eq!(macro_overlap(&macros), 0);

    macros.push(
        MacroCell {
            size: db::Rect::new((0, 0), (10, 10)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((5, 5)),
            fixed: false,
        }
    );

    assert_eq!(macro_overlap(&macros), 100);
}


/// Compute how much macro area is outside of the placement region.
fn macro_placement_region_overlap<T: CoordinateType + Ord + Bounded>(placement_regions: &Vec<db::SimpleRPolygon<T>>, macros: &Vec<MacroCell<T>>) -> T {
    let mut outside_of_placement_region = T::zero();

    // Decompose the core shape into non-overlapping rectangles.
    let placement_region_rectangles = iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles(
        placement_regions.iter()
            .flat_map(|p| p.edges())
    );

    for m in macros {
        let s = m.absolute_shape();
        let macro_area = s.width() * s.height();

        let mut overlap = T::zero();
        for r in &placement_region_rectangles {
            overlap = overlap + r.intersection(&s)
                .map(|r| r.width() * r.height())
                .unwrap_or(T::zero());
        }
        debug_assert!(macro_area >= overlap);
        let outside = macro_area - overlap;
        outside_of_placement_region = outside_of_placement_region + outside;
    }

    outside_of_placement_region
}

#[test]
fn test_macro_placement_region_overlap() {
    let macros = vec![
        MacroCell {
            size: db::Rect::new((0, 0), (10, 10)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
        MacroCell {
            size: db::Rect::new((0, 0), (10, 10)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((10, 10)),
            fixed: false,
        }
    ];

    let placement_region = vec![db::SimpleRPolygon::try_new(
        vec![(0, 0), (20, 0), (20, 20), (0, 20)]
    ).unwrap()];
    assert_eq!(macro_placement_region_overlap(&placement_region, &macros), 0);


    let placement_region = vec![db::SimpleRPolygon::try_new(
        vec![(0, 0), (15, 0), (15, 15), (0, 15)]
    ).unwrap()];
    assert_eq!(macro_placement_region_overlap(&placement_region, &macros), 75);
}

#[test]
fn test_macro_legalization() {

    // Fit four 100x100 rectangles into a 220x220 rectangle.

    let mut macros = vec![
        MacroCell {
            size: db::Rect::new((0, 0), (100, 100)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
        MacroCell {
            size: db::Rect::new((0, 0), (100, 100)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
        MacroCell {
            size: db::Rect::new((0, 0), (100, 100)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
        MacroCell {
            size: db::Rect::new((0, 0), (100, 100)),
            original_position: db::SimpleTransform::identity(),
            current_position: db::SimpleTransform::translate((0, 0)),
            fixed: false,
        },
    ];

    let s = 220;
    let placement_region = vec![db::SimpleRPolygon::try_new(
        vec![(0, 0), (s, 0), (s, s), (0, s)]
    ).unwrap()];

    let legal_positions = legalize_macros(&placement_region, &mut macros, 1000);
    assert!(legal_positions.is_some());
    dbg!(&legal_positions);
}