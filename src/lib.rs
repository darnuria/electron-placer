// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Example standard-cell placement algorithms for the LibrEDA framework.
//!
//!
//! * [`SimpleQuadraticPlacer`] does global placement of standard-cells by minimizing the quadratic wire-length.
//! It yields solutions quickly but they have strong overlaps and do not respect any density constraints.
//! Quadratic placement is therefore used mainly as an initial value for other placers.
//! * [`ElectronPlacer`] is a simple global placement algorithm driven by electrostatic repulsion between cells
//! and attracting spring forces between connected cells. Works best with an initial placement. The initial
//! placement can come for example from the [`SimpleQuadraticPlacer`]. Only standard-cell placement is supported.
//! For mixed-size placement use [`eplace_ms::EPlaceMS`].
//! * [`DiffusionPlacer`] simulates the diffusion of charges to solve density contraints. This placer
//! requires a relatively good initial placement.
//! * [`eplace_ms::EPlaceMS`] is a mixed-size placement engine which handles pre-placed macro cells
//! and complicated placement regions. It is assembled from a global placement engine and
//! macro legalization engine.
//! * [`eplace_ms::AnnealingMacroLegalizer`] legalizes macro blocks by simulated annealing and can be used
//! on its own, detached from ePlace.
//! * [`eplace_ms::MixedSizeGlobalPlacer`] can also be used on its own, detached from ePlace, for
//! global placement of macro-blocks and standard-cell.

#![deny(missing_docs)]

mod types;
mod nbody;
mod quadtree;
mod spring_force;
mod quadratic_placer;
mod electron_placer;
mod placer_argmin;
mod sparse_matrix;
mod kahan_sum;
mod diffusion_placer;

// Public exports.
pub mod eplace_ms;

pub use quadratic_placer::SimpleQuadraticPlacer;
pub use crate::electron_placer::ElectronPlacer;
pub use placer_argmin::ElectronPlacerArgmin;
pub use diffusion_placer::DiffusionPlacer;
