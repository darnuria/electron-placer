// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Compute electro-static repulsion between many particles using a quad-tree for approximation.

use rayon::prelude::*;
use super::types::*;
use super::quadtree::{Particle, QuadTree};

/// Compute electro-static repulsion between many particles using a quad-tree aproximation
/// for efficient computation.
///
/// * `particles`: All the particles.
/// * `threshold`: Accurracy setting for the quad tree. `0` is the most inaccurate `1` is very accurate but slow.
/// Decent values are around `0.5`.
pub fn calculate_electric_forces(particles: &Vec<Particle>, threshold: f64) -> Vec<Cost> {
    if particles.is_empty() {
        Vec::new()
    } else {
        // Find upper right and lower left corner by finding maxima and minima of x and y coordinates.

        let (lower_left, upper_right) =
            {
                // Use first particle as the starting value to find minimum and maximum.
                let loc0 = (particles[0].location.x, particles[0].location.y);
                // Find lower-left and upper-right corner of the bounding box around the particles.
                particles
                    .iter()
                    .fold((loc0, loc0),
                          |((xmin, ymin), (xmax, ymax)), p|
                              {
                                  let l = p.location;
                                  ((xmin.min(l.x), ymin.min(l.y)),
                                   (xmax.max(l.x), ymax.max(l.y)))
                              },
                    )
            };

        // Find minimum side length for the square-shaped quad-tree such that all particles fit in.
        let side_length = {
            let (x1, y1) = lower_left;
            let (x2, y2) = upper_right;
            (x2 - x1).max(y2 - y1)
        } * 1.001;

        // Create an empty quad-tree.
        let mut qt = QuadTree::new(lower_left, side_length);

        // Insert particles into the tree.
        for &p in particles.iter() {
            qt.insert(p);
        }

        // Pre-calculate centers of charge.
        qt.update_center_of_charge();

        // Calculate the force on each particle.
        let f_df = particles.par_iter()
            .map(|p| {
                qt.force_onto_particle(&p, threshold)
            })
            .collect();

        // Return (force, force_derivative)
        f_df
    }
}